﻿Imports System.Runtime.CompilerServices

Module ArrayExtensions

    <Extension()> _
    Public Sub ArrAppend(Of T)(ByRef arr As T(), item As T)
        Dim arr_oldlength = arr.Length - 1

        ReDim Preserve arr(arr_oldlength + 1) 'increases array length by 1
        arr(arr.Length - 1) = item 'inserts item at end

    End Sub

End Module
