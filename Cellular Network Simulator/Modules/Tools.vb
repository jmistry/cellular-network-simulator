﻿Imports System.Math
Imports System.Globalization

''' <summary>
''' A collection of methods and classes used throughout the Cellular Network Simulator
''' </summary>
''' 

Module Tools
    'Writes a file to the running directory
    Public Sub write_csv(ByVal csv_in As String, ByVal FILENAME As String)
        Dim objWriter As New System.IO.StreamWriter(FILENAME)
        objWriter.Write(csv_in)
        objWriter.Close()
    End Sub

    'Converts a power from mW to dBm
    Public Function mw_dBm(ByVal mW As Double)
        Dim dBm As Double = 10 * Log10(mW)
        Return dBm
    End Function

    'Converts a power from dBm to mW
    Public Function dBm_mW(ByVal dBm As Double)
        Dim mW As Double = Pow(10, mW / 10)
        Return mW
    End Function

    'Converts a dB gain to linear
    Public Function dB_lin(ByVal dB As Double)
        Dim lin As Double = Pow(10, dB / 10)
        Return lin
    End Function

    'Scales a coordinate (Point) by specified scaling factor
    Public Function ScalePoint(verticies() As Point, scale As Double)
        Dim new_verticies(verticies.Length - 1) As Point
        For i As Integer = 0 To verticies.Length - 1
            new_verticies(i) = New Point(verticies(i).X * scale, verticies(i).Y * scale)
        Next
        Return new_verticies
    End Function

    'Lookup table for clustering with Cluster Size N=7
    Public Function clustering_starting_band_7(ByVal column_number As Integer) As Integer
        Dim lookup_table As New Hashtable
        'This lookup table is needed to get the starting value
        'for each column of cells in the network. It takes the
        'column number as an integer and returns the 
        lookup_table(0) = 0
        lookup_table(1) = 5
        lookup_table(2) = 2
        lookup_table(3) = 0
        lookup_table(4) = 4
        lookup_table(5) = 2
        lookup_table(6) = 6
        lookup_table(7) = 4
        lookup_table(8) = 1
        lookup_table(9) = 6
        lookup_table(10) = 3
        lookup_table(11) = 1
        lookup_table(12) = 5
        lookup_table(13) = 3

        Return (lookup_table(column_number))
    End Function

    'Returns the angle of the vector AB and positive x axis (degrees)
    Public Function Angle_2P(ByVal A As Point, ByVal B As Point) As Double
        If B.X > A.X And B.Y > A.Y Then 'top-left quadrant
            Angle_2P = Atan((B.X - A.X) / (B.Y - A.Y)) / (PI / 180) + 90
        ElseIf A.X > B.X And B.Y > A.Y Then 'top-right quadrant
            Angle_2P = Atan((B.Y - A.Y) / (A.X - B.X)) / (PI / 180)
        ElseIf A.X > B.X And A.Y > B.Y Then 'bottom-right quadrant
            Angle_2P = Atan((A.X - B.X) / (A.Y - B.Y)) / (PI / 180) + 270
        ElseIf B.X > A.X And A.Y > B.Y Then ' bottom-left quadrant
            Angle_2P = Atan((A.Y - B.Y) / (B.X - A.X)) / (PI / 180) + 180
        Else
            Angle_2P = -1
        End If
    End Function

    'Returns True if a point is within a polygon
    Public Function PointInPoly_depreciated(ByVal test_point As Point, ByVal polygon() As Point) As Boolean
        Dim Angle(polygon.Length) As Double
        Dim sum_angles As Double = 0

        'Calculate angles for each point
        For x = polygon.Length - 1 To 0 Step -1
            sum_angles = 0
            For Each one_angle In Angle
                sum_angles += one_angle
            Next

            If x = polygon.Length - 1 Then 'last vertex of poly
                Angle(x) = Angle_2P(polygon(x), test_point)
            ElseIf x = 0 Then 'first vertex of poly
                Angle(x) = Angle_2P(polygon(x), test_point) - sum_angles
                Angle(polygon.Length) = -Angle_2P(polygon(x), test_point)
            Else 'all other verticies
                Angle(x) = Angle_2P(polygon(x), test_point) - sum_angles
            End If
        Next

        'Add angles together
        sum_angles = 0
        For Each one_angle In Angle
            sum_angles += one_angle
        Next

        'Determine if all angles summed to 360
        If Math.Round(sum_angles, 0) = 360 Then
            PointInPoly_depreciated = True
        Else
            PointInPoly_depreciated = False
        End If

    End Function

    'Returns the number as a string in Engineering format
    Public Function EngFormat(ByVal value As String) As String
        EngFormat = String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", value)
    End Function

    'Returns the distance (in supplied units) between two points
    Public Function DistanceTwoPoints(ByVal a As Point, ByVal b As Point) As Double
        DistanceTwoPoints = Sqrt(Pow(a.X - b.X, 2) +
                                 Pow(a.Y - b.Y, 2)
                                 )
    End Function

    'Converts a double to a string using engineering notation
    Public Function eng(ByVal num As Double, Optional ByVal sig_figs As Integer = 4) As String
        Dim m As Double = 0 'mantissa
        Dim e As Integer = 0 'exponent
        Dim num_sign As String = ""

        'Check for zero
        If num = 0 Or Double.IsNaN(num) Then
            Return num
        End If

        'Check for +infinity
        If Double.IsPositiveInfinity(num) Then
            Return "inf"
        End If

        'Check for -infinity
        If Double.IsNegativeInfinity(num) Then
            Return "-inf"
        End If

        'Check for negative numbers
        If num < 0 Then
            num = num * -1
            num_sign = "-"
        End If

        'Exponent
        e = Math.Floor(Log10(num))
        e = 3 * Math.Floor(e / 3)

        'Mantissa
        m = num / Math.Pow(10, e)

        'Determine the appropriate prefix
        Dim prefix As String = ""
        Select Case e
            Case -12
                prefix = "p"
            Case -9
                prefix = "n"
            Case -6
                prefix = "u"
            Case -3
                prefix = "m"
            Case 0
                prefix = ""
            Case 3
                prefix = "k"
            Case 6
                prefix = "M"
            Case 9
                prefix = "G"
            Case 12
                prefix = "T"
            Case 15
                prefix = "P"
        End Select

        eng = num_sign & Math.Round(m, sig_figs) & " " & prefix


    End Function

    ''' <summary>
    ''' Calculates Path Loss based on the COST-Hata Model
    ''' </summary>
    ''' <param name="f">frequency Hz</param>
    ''' <param name="h_B">basestation height m</param>
    ''' <param name="h_R">reciever height m</param>
    ''' <param name="d">distance m</param>
    ''' <param name="environment">propagation environment "suburban" or "urban"</param>
    ''' <returns>Path Loss (dB)</returns>
    ''' <remarks>
    ''' Only valid for:
    ''' h_B > h_R
    ''' f   from 150 MHz to 2000 MHz
    ''' h_R from 1 m to 10 m
    ''' h_B from 30 m to 300 m
    ''' d from 1 km to 20km
    ''' </remarks>
    Public Function COSTHata(ByVal f As Double, ByVal h_B As Double, ByVal h_R As Double, ByVal d As Double, ByVal environment As String) As Double
        'Convert frequency into MHz
        f = f / Pow(10, 6)
        'Convert distance into km
        d = d / Pow(10, 3)

        'Local variables
        Dim c_0 As Double = 0   'city size constant
        Dim c_f As Double = 0   'frequency scaling constant
        Dim a As Double = 0     'mobile station antenna height correction factor
        Dim C As Double = 0     'environment correction (suburban vs urban)

        'Define city size constant, c_0
        If 150 <= f And f < 1500 Then
            c_0 = 69.55
        ElseIf 1500 <= f And f < 2000 Then
            c_0 = 46.3
        End If

        'Define frequency scaling constant, c_f
        If 150 <= f And f < 1500 Then
            c_f = 26.16
        ElseIf 1500 <= f And f < 2000 Then
            c_f = 33.9
        End If

        'Define mobile station antenna height correction factor
        a = (1.1 * Log(f) - 0.7) * h_R - (1.56 * Log(f) - 0.8)

        'Define environment correction, C
        If environment = "suburban" Then
            C = 0
        ElseIf environment = "urban" Then
            C = 3
        End If

        'Calculate and return Loss
        COSTHata = c_0 + c_f * Log(f) + 13.82 * Log(h_B) + a + (44.9 - 6.55 * Log(h_B)) * Log(d) + C
    End Function

    Public Function StandardDeviation(ByVal input_array() As Double) As Double
        Dim mean As Double = -1
        Dim variance As Double = -1
        Dim std_deviation As Double = -1

        'Calculate mean
        Dim sum As Double = 0
        For Each element In input_array
            sum += element
        Next
        mean = sum / input_array.Count

        'Calculate variance
        Dim sum_squared_differences As Double = 0
        For Each element In input_array
            sum_squared_differences += Pow(element - mean, 2)
        Next
        variance = sum_squared_differences / input_array.Count

        'Calculate and return standard deviation
        std_deviation = Sqrt(variance)
        StandardDeviation = std_deviation

    End Function

End Module
