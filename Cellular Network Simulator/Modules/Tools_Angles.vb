﻿Imports System.Math

''' <summary>
''' A collection of routines used for the Point in Polygon problem
''' </summary>

Module Tools_Angles
    'Returns True if the point is in the polygon.
    'Reference 1997-2006 Rocky Mountain Computer Consulting, Inc.
    'Accessible at http://www.vb-helper.com/howto_net_point_in_polygon.html
    Public Function PointInPolygon(ByVal points() As Point, _
        ByVal X As Single, ByVal Y As Single) As Boolean
        ' Get the angle between the point and the
        ' first and last vertices.
        Dim max_point As Integer = points.Length - 1
        Dim total_angle As Single = GetAngle( _
            points(max_point).X, points(max_point).Y, _
            X, Y, points(0).X, points(0).Y)

        ' Add the angles from the point
        ' to each other pair of vertices.
        For i As Integer = 0 To max_point - 1
            total_angle += GetAngle( _
                points(i).X, points(i).Y, _
                X, Y, _
                points(i + 1).X, points(i + 1).Y)
        Next i

        ' The total angle should be 2 * PI or -2 * PI if
        ' the point is in the polygon and close to zero
        ' if the point is outside the polygon.
        Return Math.Abs(total_angle) > 0.000001
    End Function

    ' Return the angle with tangent opp/hyp. The returned
    ' value is between PI and -PI.
    Private Function ATan2(ByVal opp As Single, ByVal adj As _
        Single) As Single
        Dim angle As Single

        ' Get the basic angle.
        If Abs(adj) < 0.0001 Then
            angle = PI / 2
        Else
            angle = Abs(Atan(opp / adj))
        End If

        ' See if we are in quadrant 2 or 3.
        If adj < 0 Then
            ' angle > PI/2 or angle < -PI/2.
            angle = PI - angle
        End If

        ' See if we are in quadrant 3 or 4.
        If opp < 0 Then
            angle = -angle
        End If

        ' Return the result.
        ATan2 = angle
    End Function

    ' Return the angle ABC.
    ' Return a value between PI and -PI.
    ' Note that the value is the opposite of what you might
    ' expect because Y coordinates increase downward.
    Private Function GetAngle(ByVal Ax As Single, ByVal Ay As _
        Single, ByVal Bx As Single, ByVal By As Single, ByVal _
        Cx As Single, ByVal Cy As Single) As Single
        Dim dot_product As Single
        Dim cross_product As Single

        ' Get the dot product and cross product.
        dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy)
        cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, _
            Cy)

        ' Calculate the angle.
        GetAngle = ATan2(cross_product, dot_product)
    End Function

    ' Return the cross product AB x BC.
    ' The cross product is a vector perpendicular to AB
    ' and BC having length |AB| * |BC| * Sin(theta) and
    ' with direction given by the right-hand rule.
    ' For two vectors in the X-Y plane, the result is a
    ' vector with X and Y components 0 so the Z component
    ' gives the vector's length and direction.
    Public Function CrossProductLength( _
        ByVal Ax As Single, ByVal Ay As Single, _
        ByVal Bx As Single, ByVal By As Single, _
        ByVal Cx As Single, ByVal Cy As Single _
      ) As Single
        Dim BAx As Single
        Dim BAy As Single
        Dim BCx As Single
        Dim BCy As Single

        ' Get the vectors' coordinates.
        BAx = Ax - Bx
        BAy = Ay - By
        BCx = Cx - Bx
        BCy = Cy - By

        ' Calculate the Z coordinate of the cross product.
        CrossProductLength = BAx * BCy - BAy * BCx
    End Function

    ' Return the dot product AB · BC.
    ' Note that AB · BC = |AB| * |BC| * Cos(theta).
    Private Function DotProduct( _
        ByVal Ax As Single, ByVal Ay As Single, _
        ByVal Bx As Single, ByVal By As Single, _
        ByVal Cx As Single, ByVal Cy As Single _
      ) As Single
        Dim BAx As Single
        Dim BAy As Single
        Dim BCx As Single
        Dim BCy As Single

        ' Get the vectors' coordinates.
        BAx = Ax - Bx
        BAy = Ay - By
        BCx = Cx - Bx
        BCy = Cy - By

        ' Calculate the dot product.
        DotProduct = BAx * BCx + BAy * BCy
    End Function

End Module
