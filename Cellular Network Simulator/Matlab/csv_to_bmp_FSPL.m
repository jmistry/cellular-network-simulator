%-- 12/12/2013 18:29 --%
% Plot the empirical CDF of the provided dataset.
% The dataset must be as a .csv file

% Load in the data
csv_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\FSPL.csv';
bmp_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\FSPL.bmp';
FSPL = csvread(csv_fileName);

% Remove the empty data-point at the end
FSPL = FSPL(:,1:end-1);

% Plot the eCDF
[f, x] = ecdf(FSPL);
graph = figure();
plot(x, f*100);
xlabel('FSPL');
ylabel('CDF Percentage (%)');

% Save the plot
print(graph, '-dbmp', bmp_fileName);

quit