%-- 12/12/2013 18:29 --%
% Plot the empirical CDF of the provided dataset.
% The dataset must be as a .csv file

% Load in the data
csv_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\SIR.csv';
bmp_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\SIR.bmp';
SIR = csvread(csv_fileName);

% Remove the empty data-point at the end
SIR = SIR(:,1:end-1);

% Plot the eCDF
[f, x] = ecdf(SIR);
graph = figure();
plot(x, f*100);
xlabel('SIR (dB)');
ylabel('CDF Percentage (%)');

% Save the plot
print(graph, '-dbmp', bmp_fileName);

quit