%-- 12/12/2013 18:29 --%
% Plot the empirical CDF of the provided dataset.
% The dataset must be as a .csv file

% Load in the data
csv_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\SNR.csv';
bmp_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\SNR.bmp';
SNR = csvread(csv_fileName);

% Remove the empty data-point at the end
SNR = SNR(:,1:end-1);

% Plot the eCDF
[f, x] = ecdf(SNR);
graph = figure();
plot(x, f*100);
xlabel('SNR (dB)');
ylabel('CDF Percentage (%)');

% Save the plot
print(graph, '-dbmp', bmp_fileName);

quit