%-- 12/12/2013 18:29 --%
% Plot the empirical CDF of the provided dataset.
% The dataset must be as a .csv file

% Load in the data
csv_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\Capacity.csv';
bmp_fileName = 'C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\bin\Debug\Capacity.bmp';
user_capacity = csvread(csv_fileName);

% Remove the empty datapoint at the end
user_capacity = user_capacity(:,1:end-1);

% Plot the eCDF
[f, x] = ecdf(user_capacity);
graph = figure();
plot(x, f*100);
xlabel('Theoretical Capacity (M bits/s)');
ylabel('CDF Percentage (%)');

% Save the plot
print(graph, '-dbmp', bmp_fileName);

quit