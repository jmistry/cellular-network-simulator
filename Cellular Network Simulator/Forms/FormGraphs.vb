﻿Public Class FormGraphs

    Private Sub FormGraphs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set location
        Me.Top = FormMain.Top
        Me.Left = FormDrawing.Left + FormDrawing.Width
    End Sub

    Private Sub PathLossCDF()
        'Save user Path Loss to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' FSPL to .csv... ")
        Dim FSPL_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim FSPL As String = current_user.path_loss
                If FSPL = "Infinity" Then
                    FSPL = "inf"
                ElseIf FSPL = "-Infinity" Then
                    FSPL = "-inf"
                End If
                FSPL_string.AppendText(FSPL & ", ")
            End If
        Next
        Tools.write_csv(FSPL_string.Text, "FSPL.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_FSPL.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub SignalPowerCDF()
        'Save user signal power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' signal power to .csv... ")
        Dim signal_power_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim signal_power As String = current_user.signal_power_dBm
                If signal_power = "Infinity" Then
                    signal_power = "inf"
                ElseIf signal_power = "-Infinity" Then
                    signal_power = "-inf"
                End If
                signal_power_string.AppendText(signal_power & ", ")
            End If
        Next
        Tools.write_csv(signal_power_string.Text, "SignalPower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_signal_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub NoisePowerCDF()
        'Save user noise power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' noise power to .csv... ")
        Dim noise_power_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim noise_power As String = current_user.noise_power_dBm
                If noise_power = "Infinity" Then
                    noise_power = "inf"
                ElseIf noise_power = "-Infinity" Then
                    noise_power = "-inf"
                End If
                noise_power_string.AppendText(noise_power & ", ")
            End If
        Next
        Tools.write_csv(noise_power_string.Text, "NoisePower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_noise_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub InterferencePowerCDF()
        'Save user interference power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' interference power to .csv... ")
        Dim interference_power_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim interference_power As String = current_user.inteference_power_dBm
                If interference_power = "Infinity" Then
                    interference_power = "inf"
                ElseIf interference_power = "-Infinity" Then
                    interference_power = "-inf"
                End If
                interference_power_string.AppendText(interference_power & ", ")
            End If
        Next
        Tools.write_csv(interference_power_string.Text, "InterferencePower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_interference_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub SNRCDF()
        'Save user SNR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SNR to .csv... ")
        Dim SNR_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim SNR As String = current_user.SNR
                If SNR = "Infinity" Then
                    SNR = "inf"
                ElseIf SNR = "-Infinity" Then
                    SNR = "-inf"
                End If
                SNR_string.AppendText(SNR & ", ")
            End If
        Next
        Tools.write_csv(SNR_string.Text, "SNR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SNR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub SIRCDF()
        'Save user SIR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SIR to .csv... ")
        Dim SIR_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim SIR As String = current_user.SIR
                If SIR = "Infinity" Then
                    SIR = "inf"
                ElseIf SIR = "-Infinity" Then
                    SIR = "-inf"
                End If
                SIR_string.AppendText(SIR & ", ")
            End If
        Next
        Tools.write_csv(SIR_string.Text, "SIR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SIR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub SINRCDF()
        'Save user SINR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SINR to .csv... ")
        Dim SINR_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True Then 'only select users in network
                Dim SINR As String = current_user.SINR
                If SINR = "Infinity" Then
                    SINR = "inf"
                ElseIf SINR = "-Infinity" Then
                    SINR = "-inf"
                End If
                SINR_string.AppendText(SINR & ", ")
            End If
        Next
        Tools.write_csv(SINR_string.Text, "SINR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SINR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub CapacityCDF()
        'Save user capacity to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' capacity to .csv... ")
        Dim capcity_string As New TextBox
        For Each current_user In FormMain.USERS
            If current_user.interesting = True And current_user.capacity < 20 * Math.Pow(10, 6) Then 'only select users in network
                Dim capacity As String = current_user.capacity
                    If capacity = "Infinity" Then
                        capacity = "inf"
                    ElseIf capacity = "-Infinity" Then
                        capacity = "-inf"
                    End If
                    capcity_string.AppendText(capacity * Math.Pow(10, -6) & ", ")
                End If
        Next
        Tools.write_csv(capcity_string.Text, "Capacity.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Programming\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_capacity.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnGenerateGraphs_Click(sender As Object, e As EventArgs) Handles btnGenerateGraphs.Click
        'Generate graphs
        PathLossCDF()
        SignalPowerCDF()
        NoisePowerCDF()
        InterferencePowerCDF()
        SNRCDF()
        SIRCDF()
        SINRCDF()
        CapacityCDF()

        'Enable the graph chooser and saver
        ComboBoxChooseCDF.Enabled = True
        btnSaveAllGraphs.Enabled = True
    End Sub

    Private Sub ComboBoxChooseCDF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxChooseCDF.SelectedIndexChanged
        Select Case ComboBoxChooseCDF.SelectedItem
            Case "Signal Power"
                PictureBoxGraph.Image = Image.FromFile("SignalPower.bmp")
                GroupBoxGraph.Text = "Graph - Signal Power CDF"
            Case "Interference Power"
                PictureBoxGraph.Image = Image.FromFile("InterferencePower.bmp")
                GroupBoxGraph.Text = "Graph - Interferece Power CDF"
            Case "Noise Power"
                PictureBoxGraph.Image = Image.FromFile("NoisePower.bmp")
                GroupBoxGraph.Text = "Graph - Noise Power CDF"
            Case "SIR"
                PictureBoxGraph.Image = Image.FromFile("SIR.bmp")
                GroupBoxGraph.Text = "Graph - SIR CDF"
            Case "SNR"
                PictureBoxGraph.Image = Image.FromFile("SNR.bmp")
                GroupBoxGraph.Text = "Graph - SNR CDF"
            Case "SINR"
                PictureBoxGraph.Image = Image.FromFile("SINR.bmp")
                GroupBoxGraph.Text = "Graph - SINR CDF"
            Case "Capacity"
                PictureBoxGraph.Image = Image.FromFile("Capacity.bmp")
                GroupBoxGraph.Text = "Graph - Capacity CDF"
            Case Else
                PictureBoxGraph.Image = Cellular_Network_Simulator.My.Resources.Resources.BlankCDF
                GroupBoxGraph.Text = "Graph - Blank CDF"
        End Select

        'Enable the save button
        btnSaveGraph.Enabled = True
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub DebugToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DebugToolStripMenuItem.Click
        FormDebug.Show()
    End Sub

    Private Sub btnSaveGraph_Click(sender As Object, e As EventArgs) Handles btnSaveGraph.Click
        'Displays a SaveFileDialog so the user can save the displayed graph
        Dim SaveGraphDialog As New SaveFileDialog

        'Setup the SaveFileDialog
        SaveGraphDialog.Filter = "Bitmap Image|*.bmp|All Files|*.*"
        SaveGraphDialog.Title = "Save Graph"
        SaveGraphDialog.FileName = GroupBoxGraph.Text
        SaveGraphDialog.ShowDialog()

        'If the file name is not an empty string open it for saving.
        If SaveGraphDialog.FileName <> "" Then
            'Saves the Image via a FileStream created by the OpenFile method.
            Dim fs As System.IO.FileStream = CType(SaveGraphDialog.OpenFile(), System.IO.FileStream)
            PictureBoxGraph.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp)
            fs.Close()
        End If
    End Sub

    Private Sub btnSaveAllGraphs_Click(sender As Object, e As EventArgs) Handles btnSaveAllGraphs.Click
        'Displays a SaveFileDialog so the user can select a folder
        Dim FolderSelectionDialog As New FolderBrowserDialog

        'Setup the OpenFileDialog
        FolderSelectionDialog.Description = "Select a folder to save graphs to."

        'Show the OpenFileDialog and get path
        FolderSelectionDialog.ShowDialog()
        Dim folderPath As String = FolderSelectionDialog.SelectedPath

        'Record the previously selected graph item
        Dim prevSelectedIndex As Integer = ComboBoxChooseCDF.SelectedIndex

        'Iterate through graphs, saving them to chosen folder
        For index As Integer = 0 To ComboBoxChooseCDF.Items.Count - 1
            'Choose each graph
            ComboBoxChooseCDF.SelectedIndex = index

            'Save image
            Dim filePath As String = folderPath & "\" & GroupBoxGraph.Text & ".bmp"
            Dim fs As New System.IO.FileStream(filePath, IO.FileMode.Create)
            PictureBoxGraph.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp)
            fs.Close()
        Next

        'Set the comboBox back to it's previous state using prevSelectedIndex
        ComboBoxChooseCDF.SelectedIndex = prevSelectedIndex

        'Open the folder
        Process.Start("explorer.exe", folderPath)

    End Sub
End Class