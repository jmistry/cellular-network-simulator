﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSectorDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLookupUser = New System.Windows.Forms.Button()
        Me.txtUserID = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtUserList = New System.Windows.Forms.TextBox()
        Me.txtInteresting = New System.Windows.Forms.TextBox()
        Me.txtLowerFrequency = New System.Windows.Forms.TextBox()
        Me.txtUpperFrequency = New System.Windows.Forms.TextBox()
        Me.txtBandwidth = New System.Windows.Forms.TextBox()
        Me.txtUsers = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSectorNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnLookupUser
        '
        Me.btnLookupUser.Location = New System.Drawing.Point(235, 369)
        Me.btnLookupUser.Name = "btnLookupUser"
        Me.btnLookupUser.Size = New System.Drawing.Size(35, 25)
        Me.btnLookupUser.TabIndex = 37
        Me.btnLookupUser.Text = "Info"
        Me.btnLookupUser.UseVisualStyleBackColor = True
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(168, 372)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(61, 20)
        Me.txtUserID.TabIndex = 36
        Me.txtUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 375)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "User ID:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 244)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 34
        Me.Label9.Text = "User List"
        '
        'txtUserList
        '
        Me.txtUserList.Location = New System.Drawing.Point(13, 260)
        Me.txtUserList.Multiline = True
        Me.txtUserList.Name = "txtUserList"
        Me.txtUserList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtUserList.Size = New System.Drawing.Size(257, 103)
        Me.txtUserList.TabIndex = 33
        '
        'txtInteresting
        '
        Me.txtInteresting.Location = New System.Drawing.Point(170, 112)
        Me.txtInteresting.Name = "txtInteresting"
        Me.txtInteresting.Size = New System.Drawing.Size(100, 20)
        Me.txtInteresting.TabIndex = 32
        Me.txtInteresting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLowerFrequency
        '
        Me.txtLowerFrequency.Location = New System.Drawing.Point(170, 164)
        Me.txtLowerFrequency.Name = "txtLowerFrequency"
        Me.txtLowerFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtLowerFrequency.TabIndex = 31
        Me.txtLowerFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUpperFrequency
        '
        Me.txtUpperFrequency.Location = New System.Drawing.Point(170, 216)
        Me.txtUpperFrequency.Name = "txtUpperFrequency"
        Me.txtUpperFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtUpperFrequency.TabIndex = 30
        Me.txtUpperFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBandwidth
        '
        Me.txtBandwidth.Location = New System.Drawing.Point(170, 190)
        Me.txtBandwidth.Name = "txtBandwidth"
        Me.txtBandwidth.Size = New System.Drawing.Size(100, 20)
        Me.txtBandwidth.TabIndex = 29
        Me.txtBandwidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUsers
        '
        Me.txtUsers.Location = New System.Drawing.Point(170, 138)
        Me.txtUsers.Name = "txtUsers"
        Me.txtUsers.Size = New System.Drawing.Size(100, 20)
        Me.txtUsers.TabIndex = 28
        Me.txtUsers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Location x,y (m):"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(10, 115)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 26
        Me.Label12.Text = "In simulation:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 397)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(265, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "CTRL + Click on network diagram to show user details."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(10, 167)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(114, 13)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Lower Frequency (Hz):"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 219)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 13)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Upper Frequency (Hz):"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 193)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Bandwidth (Hz):"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(10, 141)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Users:"
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(170, 86)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(100, 20)
        Me.txtLocation.TabIndex = 16
        Me.txtLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(170, 8)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 20)
        Me.txtID.TabIndex = 15
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Basestation ID:"
        '
        'txtSectorNumber
        '
        Me.txtSectorNumber.Location = New System.Drawing.Point(170, 34)
        Me.txtSectorNumber.Name = "txtSectorNumber"
        Me.txtSectorNumber.Size = New System.Drawing.Size(100, 20)
        Me.txtSectorNumber.TabIndex = 16
        Me.txtSectorNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Sector Number:"
        '
        'FormSectorDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 419)
        Me.Controls.Add(Me.btnLookupUser)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtUserList)
        Me.Controls.Add(Me.txtInteresting)
        Me.Controls.Add(Me.txtLowerFrequency)
        Me.Controls.Add(Me.txtUpperFrequency)
        Me.Controls.Add(Me.txtBandwidth)
        Me.Controls.Add(Me.txtUsers)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtSectorNumber)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FormSectorDetails"
        Me.Text = "Sector ID:"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLookupUser As System.Windows.Forms.Button
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtUserList As System.Windows.Forms.TextBox
    Friend WithEvents txtInteresting As System.Windows.Forms.TextBox
    Friend WithEvents txtLowerFrequency As System.Windows.Forms.TextBox
    Friend WithEvents txtUpperFrequency As System.Windows.Forms.TextBox
    Friend WithEvents txtBandwidth As System.Windows.Forms.TextBox
    Friend WithEvents txtUsers As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSectorNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
