﻿Public Class FormDebug

    Private Sub btnPopulate_Click(sender As Object, e As EventArgs) Handles btnPopulate.Click
        For Each User In FormMain.USERS
            txtSignalPower.AppendText(User.signal_power_dBm & Environment.NewLine)
            txtNoisePower.AppendText(User.noise_power_dBm & Environment.NewLine)
            txtInterferencePower.AppendText(User.inteference_power_dBm & Environment.NewLine)
            txtSNR.AppendText(User.SNR & Environment.NewLine)
            txtSIR.AppendText(User.SIR & Environment.NewLine)
            txtSINR.AppendText(User.SINR & Environment.NewLine)
            txtCapacity.AppendText(User.capacity & Environment.NewLine)
            txtFSPL.AppendText(User.path_loss & Environment.NewLine)
        Next
    End Sub

    Private Sub btnPathLossCDF_Click(sender As Object, e As EventArgs) Handles btnPathLossCDF.Click
        'Save user Path Loss to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' FSPL to .csv... ")
        Dim FSPL_string As New TextBox
        For Each current_user In FormMain.USERS
            FSPL_string.AppendText(current_user.path_loss & ", ")
        Next
        Tools.write_csv(FSPL_string.Text, "FSPL.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_FSPL.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnSignalPowerCDF_Click(sender As Object, e As EventArgs) Handles btnSignalPowerCDF.Click
        'Save user signal power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' signal power to .csv... ")
        Dim signal_power_string As New TextBox
        For Each current_user In FormMain.USERS
            signal_power_string.AppendText(current_user.signal_power_dBm & ", ")
        Next
        Tools.write_csv(signal_power_string.Text, "SignalPower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_signal_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnNoisePowerCDF_Click(sender As Object, e As EventArgs) Handles btnNoisePowerCDF.Click
        'Save user noise power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' noise power to .csv... ")
        Dim noise_power_string As New TextBox
        For Each current_user In FormMain.USERS
            noise_power_string.AppendText(current_user.noise_power_dBm & ", ")
        Next
        Tools.write_csv(noise_power_string.Text, "NoisePower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_noise_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnInteferencePowerCDF_Click(sender As Object, e As EventArgs) Handles btnInteferencePowerCDF.Click
        'Save user interference power to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' interference power to .csv... ")
        Dim interference_power_string As New TextBox
        For Each current_user In FormMain.USERS
            interference_power_string.AppendText(current_user.inteference_power_dBm & ", ")
        Next
        Tools.write_csv(interference_power_string.Text, "InterferencePower.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_interference_power.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnSNRCDF_Click(sender As Object, e As EventArgs) Handles btnSNRCDF.Click
        'Save user SNR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SNR to .csv... ")
        Dim SNR_string As New TextBox
        For Each current_user In FormMain.USERS
            SNR_string.AppendText(current_user.SNR & ", ")
        Next
        Tools.write_csv(SNR_string.Text, "SNR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SNR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnSIRCDF_Click(sender As Object, e As EventArgs) Handles btnSIRCDF.Click
        'Save user SIR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SIR to .csv... ")
        Dim SIR_string As New TextBox
        For Each current_user In FormMain.USERS
            SIR_string.AppendText(current_user.SIR & ", ")
        Next
        Tools.write_csv(SIR_string.Text, "SIR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SIR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnSINRCDF_Click(sender As Object, e As EventArgs) Handles btnSINRCDF.Click
        'Save user SINR to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' SINR to .csv... ")
        Dim SINR_string As New TextBox
        For Each current_user In FormMain.USERS
            SINR_string.AppendText(current_user.SINR & ", ")
        Next
        Tools.write_csv(SINR_string.Text, "SINR.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_SINR.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub btnCapacityCDF_Click(sender As Object, e As EventArgs) Handles btnCapacityCDF.Click
        'Save user capacity to .csv file
        FormMain.txtAppStatus.AppendText("Saving users' capacity to .csv... ")
        Dim capcity_string As New TextBox
        For Each current_user In FormMain.USERS
            capcity_string.AppendText(current_user.capacity & ", ")
        Next
        Tools.write_csv(capcity_string.Text, "Capacity.csv")
        FormMain.txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate a bmp of the .csv file
        Dim p As New ProcessStartInfo
        p.FileName = "matlab"
        p.Arguments = "-nodisplay -nosplash -nodesktop -r ""run('C:\Users\Jaimesh\Dropbox\University\Bristol\Courses\Year 4\Fourth Year Project\cellular-network-simulator\Cellular Network Simulator\Matlab\csv_to_bmp_capacity.m');"""
        p.WindowStyle = ProcessWindowStyle.Hidden
        p.CreateNoWindow = True
        p.UseShellExecute = False
        Process.Start(p)
    End Sub

    Private Sub FormDebug_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set location
        Me.Left = FormMain.Left
        Me.Top = FormMain.Top
    End Sub
End Class