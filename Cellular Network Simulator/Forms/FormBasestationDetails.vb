﻿Imports System.Math

Public Class FormBasestationDetails

    Public Sub New(ByVal cell As Basestation)
        InitializeComponent()

        Me.Text = "Basestation ID: " & cell.ID
        Me.txtID.Text = cell.ID
        Me.txtLocation.Text = cell.center_point(0).X & ", " & cell.center_point(0).Y
        Me.txtInteresting.Text = cell.interesting
        Me.txtCellRadius.Text = cell.cell_radius
        Me.txtHorizontalSectors.Text = CInt(cell.sectors.Count)
        Me.txtUsers.Text = cell.users_IDs.Length
        Me.txtBandwidth.Text = Tools.eng(cell.band.bandwidth)
        Me.txtLowerFrequency.Text = Tools.eng(cell.band.lower_frequency)
        Me.txtUpperFrequency.Text = Tools.eng(cell.band.upper_frequency)
        For Each UserID In cell.users_IDs
            Me.txtUserList.AppendText(UserID & ", ")
        Next
    End Sub

    Private Sub btnLookupUser_Click(sender As Object, e As EventArgs) Handles btnLookupUser.Click
        Dim selected_user As New User(0, New Point(0, 0), 0)
        selected_user = FormMain.USERS(txtUserID.Text)

        Dim details_form As New FormUserDetails(selected_user)
        details_form.Show()

    End Sub

End Class