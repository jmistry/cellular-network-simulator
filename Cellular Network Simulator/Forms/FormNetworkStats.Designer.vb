﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormNetworkStats
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormNetworkStats))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblUsersInSimCount = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblBasestationsInSimCount = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblNetworkCapacity = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblSpectrumEfficiency = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblOutageProbability = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblUserCapacityPeak = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblUserCapacityMean = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblUserCapacityStdDev = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblUserCapacityMedian = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Users in simulation:"
        '
        'lblUsersInSimCount
        '
        Me.lblUsersInSimCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUsersInSimCount.Location = New System.Drawing.Point(154, 9)
        Me.lblUsersInSimCount.Name = "lblUsersInSimCount"
        Me.lblUsersInSimCount.Size = New System.Drawing.Size(83, 13)
        Me.lblUsersInSimCount.TabIndex = 1
        Me.lblUsersInSimCount.Text = "0 / 0"
        Me.lblUsersInSimCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Basestations in simulation:"
        '
        'lblBasestationsInSimCount
        '
        Me.lblBasestationsInSimCount.Location = New System.Drawing.Point(148, 22)
        Me.lblBasestationsInSimCount.Name = "lblBasestationsInSimCount"
        Me.lblBasestationsInSimCount.Size = New System.Drawing.Size(89, 13)
        Me.lblBasestationsInSimCount.TabIndex = 3
        Me.lblBasestationsInSimCount.Text = "0 / 0"
        Me.lblBasestationsInSimCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Network Capacity:"
        '
        'lblNetworkCapacity
        '
        Me.lblNetworkCapacity.Location = New System.Drawing.Point(151, 35)
        Me.lblNetworkCapacity.Name = "lblNetworkCapacity"
        Me.lblNetworkCapacity.Size = New System.Drawing.Size(86, 13)
        Me.lblNetworkCapacity.TabIndex = 5
        Me.lblNetworkCapacity.Text = "0"
        Me.lblNetworkCapacity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(234, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "bits/s"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Network Spectral Efficiency:"
        '
        'lblSpectrumEfficiency
        '
        Me.lblSpectrumEfficiency.Location = New System.Drawing.Point(151, 48)
        Me.lblSpectrumEfficiency.Name = "lblSpectrumEfficiency"
        Me.lblSpectrumEfficiency.Size = New System.Drawing.Size(86, 13)
        Me.lblSpectrumEfficiency.TabIndex = 5
        Me.lblSpectrumEfficiency.Text = "0"
        Me.lblSpectrumEfficiency.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(234, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "bits/s/Hz"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(322, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Outage Probablility:"
        '
        'lblOutageProbability
        '
        Me.lblOutageProbability.Location = New System.Drawing.Point(461, 73)
        Me.lblOutageProbability.Name = "lblOutageProbability"
        Me.lblOutageProbability.Size = New System.Drawing.Size(86, 13)
        Me.lblOutageProbability.TabIndex = 5
        Me.lblOutageProbability.Text = "0"
        Me.lblOutageProbability.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(553, 73)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(15, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "%"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(322, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(110, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "User Capacity - Peak:"
        '
        'lblUserCapacityPeak
        '
        Me.lblUserCapacityPeak.Location = New System.Drawing.Point(461, 9)
        Me.lblUserCapacityPeak.Name = "lblUserCapacityPeak"
        Me.lblUserCapacityPeak.Size = New System.Drawing.Size(86, 13)
        Me.lblUserCapacityPeak.TabIndex = 5
        Me.lblUserCapacityPeak.Text = "0"
        Me.lblUserCapacityPeak.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(553, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(33, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "bits/s"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(322, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(112, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "User Capacity - Mean:"
        '
        'lblUserCapacityMean
        '
        Me.lblUserCapacityMean.Location = New System.Drawing.Point(461, 22)
        Me.lblUserCapacityMean.Name = "lblUserCapacityMean"
        Me.lblUserCapacityMean.Size = New System.Drawing.Size(86, 13)
        Me.lblUserCapacityMean.TabIndex = 5
        Me.lblUserCapacityMean.Text = "0"
        Me.lblUserCapacityMean.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(553, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 13)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "bits/s"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(322, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(154, 13)
        Me.Label15.TabIndex = 4
        Me.Label15.Text = "User Capacity - Standard Dev.:"
        '
        'lblUserCapacityStdDev
        '
        Me.lblUserCapacityStdDev.Location = New System.Drawing.Point(476, 48)
        Me.lblUserCapacityStdDev.Name = "lblUserCapacityStdDev"
        Me.lblUserCapacityStdDev.Size = New System.Drawing.Size(71, 13)
        Me.lblUserCapacityStdDev.TabIndex = 5
        Me.lblUserCapacityStdDev.Text = "0"
        Me.lblUserCapacityStdDev.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(553, 48)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(33, 13)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "bits/s"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(322, 35)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(156, 13)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "User Capacity - 50th Percentile:"
        '
        'lblUserCapacityMedian
        '
        Me.lblUserCapacityMedian.Location = New System.Drawing.Point(479, 35)
        Me.lblUserCapacityMedian.Name = "lblUserCapacityMedian"
        Me.lblUserCapacityMedian.Size = New System.Drawing.Size(68, 13)
        Me.lblUserCapacityMedian.TabIndex = 5
        Me.lblUserCapacityMedian.Text = "0"
        Me.lblUserCapacityMedian.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(553, 35)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(33, 13)
        Me.Label21.TabIndex = 6
        Me.Label21.Text = "bits/s"
        '
        'FormNetworkStats
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 112)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblSpectrumEfficiency)
        Me.Controls.Add(Me.lblUserCapacityStdDev)
        Me.Controls.Add(Me.lblUserCapacityMedian)
        Me.Controls.Add(Me.lblUserCapacityMean)
        Me.Controls.Add(Me.lblUserCapacityPeak)
        Me.Controls.Add(Me.lblOutageProbability)
        Me.Controls.Add(Me.lblNetworkCapacity)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblBasestationsInSimCount)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblUsersInSimCount)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormNetworkStats"
        Me.Text = "Cellular Network Simulator - Network Stats"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblUsersInSimCount As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblBasestationsInSimCount As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblNetworkCapacity As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblSpectrumEfficiency As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblOutageProbability As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblUserCapacityPeak As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblUserCapacityMean As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblUserCapacityStdDev As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblUserCapacityMedian As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
End Class
