﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormGraphs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGraphs))
        Me.btnGenerateGraphs = New System.Windows.Forms.Button()
        Me.GroupBoxGraph = New System.Windows.Forms.GroupBox()
        Me.PictureBoxGraph = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveGraphsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebugToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSaveGraph = New System.Windows.Forms.Button()
        Me.btnSaveAllGraphs = New System.Windows.Forms.Button()
        Me.ComboBoxChooseCDF = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBoxGraph.SuspendLayout()
        CType(Me.PictureBoxGraph, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGenerateGraphs
        '
        Me.btnGenerateGraphs.Location = New System.Drawing.Point(12, 45)
        Me.btnGenerateGraphs.Name = "btnGenerateGraphs"
        Me.btnGenerateGraphs.Size = New System.Drawing.Size(340, 39)
        Me.btnGenerateGraphs.TabIndex = 1
        Me.btnGenerateGraphs.Text = "Generate Graphs"
        Me.btnGenerateGraphs.UseVisualStyleBackColor = True
        '
        'GroupBoxGraph
        '
        Me.GroupBoxGraph.Controls.Add(Me.PictureBoxGraph)
        Me.GroupBoxGraph.Location = New System.Drawing.Point(12, 112)
        Me.GroupBoxGraph.Name = "GroupBoxGraph"
        Me.GroupBoxGraph.Size = New System.Drawing.Size(660, 509)
        Me.GroupBoxGraph.TabIndex = 2
        Me.GroupBoxGraph.TabStop = False
        Me.GroupBoxGraph.Text = "Graph - Blank CDF"
        '
        'PictureBoxGraph
        '
        Me.PictureBoxGraph.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBoxGraph.Image = Global.Cellular_Network_Simulator.My.Resources.Resources.BlankCDF
        Me.PictureBoxGraph.Location = New System.Drawing.Point(3, 16)
        Me.PictureBoxGraph.Name = "PictureBoxGraph"
        Me.PictureBoxGraph.Size = New System.Drawing.Size(654, 490)
        Me.PictureBoxGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxGraph.TabIndex = 0
        Me.PictureBoxGraph.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(684, 24)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveGraphsToolStripMenuItem, Me.ToolStripMenuItem1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'SaveGraphsToolStripMenuItem
        '
        Me.SaveGraphsToolStripMenuItem.Name = "SaveGraphsToolStripMenuItem"
        Me.SaveGraphsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveGraphsToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.SaveGraphsToolStripMenuItem.Text = "&Save Graphs"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(175, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebugToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.ToolsToolStripMenuItem.Text = "&Tools"
        '
        'DebugToolStripMenuItem
        '
        Me.DebugToolStripMenuItem.Name = "DebugToolStripMenuItem"
        Me.DebugToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.DebugToolStripMenuItem.Text = "&Debug"
        '
        'btnSaveGraph
        '
        Me.btnSaveGraph.Enabled = False
        Me.btnSaveGraph.Location = New System.Drawing.Point(489, 627)
        Me.btnSaveGraph.Name = "btnSaveGraph"
        Me.btnSaveGraph.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveGraph.TabIndex = 5
        Me.btnSaveGraph.Text = "Save Graph"
        Me.btnSaveGraph.UseVisualStyleBackColor = True
        '
        'btnSaveAllGraphs
        '
        Me.btnSaveAllGraphs.Enabled = False
        Me.btnSaveAllGraphs.Location = New System.Drawing.Point(573, 627)
        Me.btnSaveAllGraphs.Name = "btnSaveAllGraphs"
        Me.btnSaveAllGraphs.Size = New System.Drawing.Size(99, 23)
        Me.btnSaveAllGraphs.TabIndex = 6
        Me.btnSaveAllGraphs.Text = "Save All Graphs"
        Me.btnSaveAllGraphs.UseVisualStyleBackColor = True
        '
        'ComboBoxChooseCDF
        '
        Me.ComboBoxChooseCDF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxChooseCDF.Enabled = False
        Me.ComboBoxChooseCDF.FormattingEnabled = True
        Me.ComboBoxChooseCDF.Items.AddRange(New Object() {"Signal Power", "Noise Power", "Interference Power", "SNR", "SIR", "SINR", "Capacity"})
        Me.ComboBoxChooseCDF.Location = New System.Drawing.Point(49, 627)
        Me.ComboBoxChooseCDF.Name = "ComboBoxChooseCDF"
        Me.ComboBoxChooseCDF.Size = New System.Drawing.Size(129, 21)
        Me.ComboBoxChooseCDF.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 632)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "CDF:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(358, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(314, 39)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "NOTE: The graph generation button opens several instances of " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MATLAB, generates " & _
    "graphs and then closes. This process takes " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "around 2 minutes. Please wait for t" & _
    "his to complete."
        '
        'FormGraphs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 662)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxChooseCDF)
        Me.Controls.Add(Me.btnSaveAllGraphs)
        Me.Controls.Add(Me.btnSaveGraph)
        Me.Controls.Add(Me.GroupBoxGraph)
        Me.Controls.Add(Me.btnGenerateGraphs)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FormGraphs"
        Me.Text = "Cellular Network Simulator - Results"
        Me.GroupBoxGraph.ResumeLayout(False)
        CType(Me.PictureBoxGraph, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBoxGraph As System.Windows.Forms.PictureBox
    Friend WithEvents btnGenerateGraphs As System.Windows.Forms.Button
    Friend WithEvents GroupBoxGraph As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveGraphsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DebugToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSaveGraph As System.Windows.Forms.Button
    Friend WithEvents btnSaveAllGraphs As System.Windows.Forms.Button
    Friend WithEvents ComboBoxChooseCDF As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
