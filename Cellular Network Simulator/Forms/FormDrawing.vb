﻿Public Class FormDrawing

    Private Sub FormDrawing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Move to righto of FormMain
        Me.Top = FormMain.Top
        Me.Left = FormMain.Left + FormMain.Width

        'Show FormNetworkStats
        FormNetworkStats.Show()
    End Sub

    Private Sub ShowSectorInfo()
        Dim cursor_position(0) As Point
        cursor_position(0) = Me.PointToClient(Control.MousePosition) 'grab mouse position
        
        'Unscale point
        cursor_position = ScalePoint(cursor_position, 1 / FormMain.NETWORK.scaling_factor)

        'Find which basestation cursor is in using PointInPolygon()
        Dim Basestation_clicked As New Basestation(-1, New Point(-1, -1), 0, 0, 0) 'create null Basestation
        For Each Basestation In FormMain.BASESTATIONS
            If PointInPolygon(Basestation.hexagon_verticies, cursor_position(0).X, cursor_position(0).Y) Then
                Basestation_clicked = Basestation
            End If
        Next

        'Find which sector cursor is in using PointInPolygon()
        Dim Sector_clicked As New Sector(New Bandwidth(-1, -1), New Point(-1, -1), New Point(-1, -1), New Point(-1, -1), New Point(-1, -1))
        For Each Sector In Basestation_clicked.sectors
            If PointInPolygon(Sector.verticies, cursor_position(0).X, cursor_position(0).Y) Then
                Sector_clicked = Sector
            End If
        Next

        'Display cell information
        If Basestation_clicked.ID <> -1 Then
            Dim details_form As New FormSectorDetails(Basestation_clicked, Sector_clicked)
            details_form.Show()
        End If
    End Sub

    Private Sub ShowBasestationInfo()
        Dim cursor_position(0) As Point
        cursor_position(0) = Me.PointToClient(Control.MousePosition) 'grab mouse position
        'MsgBox("Cursor form position = (" & cursor_position(0).X & "," & cursor_position(0).Y & ")")

        'Unscale point
        cursor_position = ScalePoint(cursor_position, 1 / FormMain.NETWORK.scaling_factor)

        'Find which cell cursor is in using PointInPolygon()
        Dim Basestation_clicked As New Basestation(-1, New Point(-1, -1), 0, 0, 0) 'create null Basestation
        For Each Basestation In FormMain.BASESTATIONS
            If PointInPolygon(Basestation.hexagon_verticies, cursor_position(0).X, cursor_position(0).Y) Then
                Basestation_clicked = Basestation
            End If
        Next

        'Display cell information
        If Basestation_clicked.ID <> -1 Then
            Dim details_form As New FormBasestationDetails(Basestation_clicked)
            details_form.Show()
        End If
    End Sub

    Private Sub ShowUserInfo()
        Dim cursor_position(0) As Point
        cursor_position(0) = Me.PointToClient(Control.MousePosition) 'grab mouse position
        'MsgBox("Cursor form position = (" & cursor_position(0).X & "," & cursor_position(0).Y & ")")

        'Unscale point
        cursor_position = ScalePoint(cursor_position, 1 / FormMain.NETWORK.scaling_factor)

        'Find which user the cursor is closest to
        Dim user_clicked As New User(-1, New Point(-1, -1), -1)
        Dim distance As Double = FormMain.NETWORK.size.X 'largest distance in network
        For Each User In FormMain.USERS
            If DistanceTwoPoints(User.center_point, cursor_position(0)) < distance Then
                distance = DistanceTwoPoints(User.center_point, cursor_position(0))
                user_clicked = User
            End If
        Next

        'Display user information
        If user_clicked.ID <> -1 Then
            Dim details_form As New FormUserDetails(user_clicked)
            details_form.Show()
        End If
    End Sub

    Private Sub PanelNetworkDrawing_Click(sender As Object, e As EventArgs) Handles PanelNetworkDrawing.Click
        If Control.ModifierKeys = Keys.Control Then
            ShowUserInfo()
        Else
            If FormMain.place_hotspot_mode = True Then
                Dim cursor_position(0) As Point
                cursor_position(0) = Me.PointToClient(Control.MousePosition) 'grab mouse position

                'Draw on form
                Dim diameter As Double = FormMain.txtHotspotRadius.Text * (FormMain.NETWORK.scaling_factor) * 2
                Dim size As New Size(diameter, diameter)
                Dim position As New Point(cursor_position(0).X - diameter / 2, cursor_position(0).Y - diameter / 2)
                Dim circle As New Rectangle(position, size)
                Me.PanelNetworkDrawing.CreateGraphics.DrawEllipse(Pens.Green, circle)

                'Create hotspot
                cursor_position = ScalePoint(cursor_position, 1 / FormMain.NETWORK.scaling_factor) 'scale point
                Dim new_hotspot As New Hotspot(cursor_position,
                                            FormMain.txtHotspotRadius.Text,
                                            FormMain.numHotspotUsers.Value)

                'Add hotspot
                ArrAppend(FormMain.HOTSPOTS, new_hotspot)

                'Reset
                FormMain.place_hotspot_mode = False
                Me.Cursor = Cursors.Default
            Else
                If FormMain.NETWORK.sectoring = True Then
                    ShowSectorInfo()
                Else
                    ShowBasestationInfo()
                End If
            End If

        End If
    End Sub


End Class