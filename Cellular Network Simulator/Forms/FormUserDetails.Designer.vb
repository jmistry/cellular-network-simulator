﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormUserDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormUserDetails))
        Me.txtLowerFrequency = New System.Windows.Forms.TextBox()
        Me.txtUpperFrequency = New System.Windows.Forms.TextBox()
        Me.txtBandwidth = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.v = New System.Windows.Forms.Label()
        Me.txtServingBasestation = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSignalPower = New System.Windows.Forms.TextBox()
        Me.txtNoisePower = New System.Windows.Forms.TextBox()
        Me.txtInterferencePower = New System.Windows.Forms.TextBox()
        Me.txtSINR = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCapacity = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnBasestationInfo = New System.Windows.Forms.Button()
        Me.txtInteresting = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtLowerFrequency
        '
        Me.txtLowerFrequency.Location = New System.Drawing.Point(172, 90)
        Me.txtLowerFrequency.Name = "txtLowerFrequency"
        Me.txtLowerFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtLowerFrequency.TabIndex = 21
        Me.txtLowerFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUpperFrequency
        '
        Me.txtUpperFrequency.Location = New System.Drawing.Point(172, 142)
        Me.txtUpperFrequency.Name = "txtUpperFrequency"
        Me.txtUpperFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtUpperFrequency.TabIndex = 22
        Me.txtUpperFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBandwidth
        '
        Me.txtBandwidth.Location = New System.Drawing.Point(172, 116)
        Me.txtBandwidth.Name = "txtBandwidth"
        Me.txtBandwidth.Size = New System.Drawing.Size(100, 20)
        Me.txtBandwidth.TabIndex = 23
        Me.txtBandwidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Location x,y (m):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 93)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(114, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Lower Frequency (Hz):"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 145)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Upper Frequency (Hz):"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 119)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Bandwidth (Hz):"
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(172, 38)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(100, 20)
        Me.txtLocation.TabIndex = 11
        Me.txtLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(172, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 20)
        Me.txtID.TabIndex = 10
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'v
        '
        Me.v.AutoSize = True
        Me.v.Location = New System.Drawing.Point(12, 15)
        Me.v.Name = "v"
        Me.v.Size = New System.Drawing.Size(46, 13)
        Me.v.TabIndex = 9
        Me.v.Text = "User ID:"
        '
        'txtServingBasestation
        '
        Me.txtServingBasestation.Location = New System.Drawing.Point(172, 298)
        Me.txtServingBasestation.Name = "txtServingBasestation"
        Me.txtServingBasestation.Size = New System.Drawing.Size(61, 20)
        Me.txtServingBasestation.TabIndex = 24
        Me.txtServingBasestation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 301)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Serving Basestation:"
        '
        'txtSignalPower
        '
        Me.txtSignalPower.Location = New System.Drawing.Point(172, 168)
        Me.txtSignalPower.Name = "txtSignalPower"
        Me.txtSignalPower.Size = New System.Drawing.Size(100, 20)
        Me.txtSignalPower.TabIndex = 26
        Me.txtSignalPower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNoisePower
        '
        Me.txtNoisePower.Location = New System.Drawing.Point(172, 194)
        Me.txtNoisePower.Name = "txtNoisePower"
        Me.txtNoisePower.Size = New System.Drawing.Size(100, 20)
        Me.txtNoisePower.TabIndex = 27
        Me.txtNoisePower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterferencePower
        '
        Me.txtInterferencePower.Location = New System.Drawing.Point(172, 220)
        Me.txtInterferencePower.Name = "txtInterferencePower"
        Me.txtInterferencePower.Size = New System.Drawing.Size(100, 20)
        Me.txtInterferencePower.TabIndex = 28
        Me.txtInterferencePower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSINR
        '
        Me.txtSINR.Location = New System.Drawing.Point(172, 246)
        Me.txtSINR.Name = "txtSINR"
        Me.txtSINR.Size = New System.Drawing.Size(100, 20)
        Me.txtSINR.TabIndex = 29
        Me.txtSINR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 197)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Noise Power (dBm):"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 171)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Signal Power (dBm):"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 223)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Interference Power (dBm):"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 249)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "SINR (dB):"
        '
        'txtCapacity
        '
        Me.txtCapacity.Location = New System.Drawing.Point(172, 272)
        Me.txtCapacity.Name = "txtCapacity"
        Me.txtCapacity.Size = New System.Drawing.Size(100, 20)
        Me.txtCapacity.TabIndex = 29
        Me.txtCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 275)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Capacity (bits/s):"
        '
        'btnBasestationInfo
        '
        Me.btnBasestationInfo.Location = New System.Drawing.Point(239, 296)
        Me.btnBasestationInfo.Name = "btnBasestationInfo"
        Me.btnBasestationInfo.Size = New System.Drawing.Size(33, 23)
        Me.btnBasestationInfo.TabIndex = 30
        Me.btnBasestationInfo.Text = "Info"
        Me.btnBasestationInfo.UseVisualStyleBackColor = True
        '
        'txtInteresting
        '
        Me.txtInteresting.Location = New System.Drawing.Point(172, 64)
        Me.txtInteresting.Name = "txtInteresting"
        Me.txtInteresting.Size = New System.Drawing.Size(100, 20)
        Me.txtInteresting.TabIndex = 32
        Me.txtInteresting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 31
        Me.Label11.Text = "In simulation:"
        '
        'FormUserDetails
        '
        Me.AcceptButton = Me.btnBasestationInfo
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 332)
        Me.Controls.Add(Me.txtInteresting)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.btnBasestationInfo)
        Me.Controls.Add(Me.txtCapacity)
        Me.Controls.Add(Me.txtSINR)
        Me.Controls.Add(Me.txtInterferencePower)
        Me.Controls.Add(Me.txtNoisePower)
        Me.Controls.Add(Me.txtSignalPower)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtServingBasestation)
        Me.Controls.Add(Me.txtLowerFrequency)
        Me.Controls.Add(Me.txtUpperFrequency)
        Me.Controls.Add(Me.txtBandwidth)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.v)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormUserDetails"
        Me.Text = "User ID: "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtLowerFrequency As System.Windows.Forms.TextBox
    Friend WithEvents txtUpperFrequency As System.Windows.Forms.TextBox
    Friend WithEvents txtBandwidth As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents v As System.Windows.Forms.Label
    Friend WithEvents txtServingBasestation As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSignalPower As System.Windows.Forms.TextBox
    Friend WithEvents txtNoisePower As System.Windows.Forms.TextBox
    Friend WithEvents txtInterferencePower As System.Windows.Forms.TextBox
    Friend WithEvents txtSINR As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCapacity As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnBasestationInfo As System.Windows.Forms.Button
    Friend WithEvents txtInteresting As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
