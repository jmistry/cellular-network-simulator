﻿Imports System.Math

Public Class FormUserDetails

    Public Sub New(ByVal user As User)
        InitializeComponent()

        Me.Text = "User ID: " & user.ID
        Me.txtID.Text = user.ID
        Me.txtServingBasestation.Text = user.serving_basestation_ID
        Me.txtInteresting.Text = user.interesting
        Me.txtLocation.Text = user.center_point.X & ", " & user.center_point.Y
        Me.txtBandwidth.Text = Tools.eng(user.bandwidth.bandwidth, 8)
        Me.txtLowerFrequency.Text = Tools.eng(user.bandwidth.lower_frequency, 8)
        Me.txtUpperFrequency.Text = Tools.eng(user.bandwidth.upper_frequency, 8)
        Me.txtSignalPower.Text = Math.Round(user.signal_power_dBm, 3)
        Me.txtNoisePower.Text = Math.Round(user.noise_power_dBm, 3)
        Me.txtInterferencePower.Text = Math.Round(user.inteference_power_dBm, 3)
        Me.txtSINR.Text = Math.Round(user.SINR, 3)
        Me.txtCapacity.Text = Tools.eng(user.capacity)
    End Sub

    Private Sub btnBasestationInfo_Click(sender As Object, e As EventArgs) Handles btnBasestationInfo.Click
        Dim details_form As New FormBasestationDetails(FormMain.BASESTATIONS(txtServingBasestation.Text))
        details_form.Show()
    End Sub
End Class