﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDebug
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormDebug))
        Me.btnPopulate = New System.Windows.Forms.Button()
        Me.txtSignalPower = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSignalPowerCDF = New System.Windows.Forms.Button()
        Me.btnNoisePowerCDF = New System.Windows.Forms.Button()
        Me.lblNoise = New System.Windows.Forms.Label()
        Me.txtNoisePower = New System.Windows.Forms.TextBox()
        Me.btnInteferencePowerCDF = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtInterferencePower = New System.Windows.Forms.TextBox()
        Me.btnSNRCDF = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSNR = New System.Windows.Forms.TextBox()
        Me.btnSIRCDF = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtSIR = New System.Windows.Forms.TextBox()
        Me.btnSINRCDF = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtSINR = New System.Windows.Forms.TextBox()
        Me.btnCapacityCDF = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCapacity = New System.Windows.Forms.TextBox()
        Me.btnPathLossCDF = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFSPL = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnPopulate
        '
        Me.btnPopulate.Location = New System.Drawing.Point(13, 13)
        Me.btnPopulate.Name = "btnPopulate"
        Me.btnPopulate.Size = New System.Drawing.Size(75, 23)
        Me.btnPopulate.TabIndex = 0
        Me.btnPopulate.Text = "Populate"
        Me.btnPopulate.UseVisualStyleBackColor = True
        '
        'txtSignalPower
        '
        Me.txtSignalPower.Location = New System.Drawing.Point(206, 89)
        Me.txtSignalPower.Multiline = True
        Me.txtSignalPower.Name = "txtSignalPower"
        Me.txtSignalPower.ReadOnly = True
        Me.txtSignalPower.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSignalPower.Size = New System.Drawing.Size(184, 456)
        Me.txtSignalPower.TabIndex = 14
        Me.txtSignalPower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(205, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Signal Power dBm"
        '
        'btnSignalPowerCDF
        '
        Me.btnSignalPowerCDF.Location = New System.Drawing.Point(206, 551)
        Me.btnSignalPowerCDF.Name = "btnSignalPowerCDF"
        Me.btnSignalPowerCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnSignalPowerCDF.TabIndex = 16
        Me.btnSignalPowerCDF.Text = "Signal Power CDF"
        Me.btnSignalPowerCDF.UseVisualStyleBackColor = True
        '
        'btnNoisePowerCDF
        '
        Me.btnNoisePowerCDF.Location = New System.Drawing.Point(396, 551)
        Me.btnNoisePowerCDF.Name = "btnNoisePowerCDF"
        Me.btnNoisePowerCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnNoisePowerCDF.TabIndex = 19
        Me.btnNoisePowerCDF.Text = "Noise Power CDF"
        Me.btnNoisePowerCDF.UseVisualStyleBackColor = True
        '
        'lblNoise
        '
        Me.lblNoise.AutoSize = True
        Me.lblNoise.Location = New System.Drawing.Point(395, 73)
        Me.lblNoise.Name = "lblNoise"
        Me.lblNoise.Size = New System.Drawing.Size(91, 13)
        Me.lblNoise.TabIndex = 18
        Me.lblNoise.Text = "Noise Power dBm"
        '
        'txtNoisePower
        '
        Me.txtNoisePower.Location = New System.Drawing.Point(396, 89)
        Me.txtNoisePower.Multiline = True
        Me.txtNoisePower.Name = "txtNoisePower"
        Me.txtNoisePower.ReadOnly = True
        Me.txtNoisePower.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNoisePower.Size = New System.Drawing.Size(184, 456)
        Me.txtNoisePower.TabIndex = 17
        Me.txtNoisePower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnInteferencePowerCDF
        '
        Me.btnInteferencePowerCDF.Location = New System.Drawing.Point(588, 551)
        Me.btnInteferencePowerCDF.Name = "btnInteferencePowerCDF"
        Me.btnInteferencePowerCDF.Size = New System.Drawing.Size(182, 23)
        Me.btnInteferencePowerCDF.TabIndex = 22
        Me.btnInteferencePowerCDF.Text = "Interference Power CDF"
        Me.btnInteferencePowerCDF.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(585, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Interference Power dBm"
        '
        'txtInterferencePower
        '
        Me.txtInterferencePower.Location = New System.Drawing.Point(586, 89)
        Me.txtInterferencePower.Multiline = True
        Me.txtInterferencePower.Name = "txtInterferencePower"
        Me.txtInterferencePower.ReadOnly = True
        Me.txtInterferencePower.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInterferencePower.Size = New System.Drawing.Size(184, 456)
        Me.txtInterferencePower.TabIndex = 20
        Me.txtInterferencePower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnSNRCDF
        '
        Me.btnSNRCDF.Location = New System.Drawing.Point(776, 551)
        Me.btnSNRCDF.Name = "btnSNRCDF"
        Me.btnSNRCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnSNRCDF.TabIndex = 25
        Me.btnSNRCDF.Text = "SNR CDF"
        Me.btnSNRCDF.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(775, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "SNR"
        '
        'txtSNR
        '
        Me.txtSNR.Location = New System.Drawing.Point(776, 89)
        Me.txtSNR.Multiline = True
        Me.txtSNR.Name = "txtSNR"
        Me.txtSNR.ReadOnly = True
        Me.txtSNR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSNR.Size = New System.Drawing.Size(184, 456)
        Me.txtSNR.TabIndex = 23
        Me.txtSNR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnSIRCDF
        '
        Me.btnSIRCDF.Location = New System.Drawing.Point(966, 551)
        Me.btnSIRCDF.Name = "btnSIRCDF"
        Me.btnSIRCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnSIRCDF.TabIndex = 28
        Me.btnSIRCDF.Text = "SIR CDF"
        Me.btnSIRCDF.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(965, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 13)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "SIR"
        '
        'txtSIR
        '
        Me.txtSIR.Location = New System.Drawing.Point(966, 89)
        Me.txtSIR.Multiline = True
        Me.txtSIR.Name = "txtSIR"
        Me.txtSIR.ReadOnly = True
        Me.txtSIR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSIR.Size = New System.Drawing.Size(184, 456)
        Me.txtSIR.TabIndex = 26
        Me.txtSIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnSINRCDF
        '
        Me.btnSINRCDF.Location = New System.Drawing.Point(1156, 551)
        Me.btnSINRCDF.Name = "btnSINRCDF"
        Me.btnSINRCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnSINRCDF.TabIndex = 31
        Me.btnSINRCDF.Text = "SINR CDF"
        Me.btnSINRCDF.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1155, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "SINR"
        '
        'txtSINR
        '
        Me.txtSINR.Location = New System.Drawing.Point(1156, 89)
        Me.txtSINR.Multiline = True
        Me.txtSINR.Name = "txtSINR"
        Me.txtSINR.ReadOnly = True
        Me.txtSINR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSINR.Size = New System.Drawing.Size(184, 456)
        Me.txtSINR.TabIndex = 29
        Me.txtSINR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnCapacityCDF
        '
        Me.btnCapacityCDF.Location = New System.Drawing.Point(1346, 551)
        Me.btnCapacityCDF.Name = "btnCapacityCDF"
        Me.btnCapacityCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnCapacityCDF.TabIndex = 34
        Me.btnCapacityCDF.Text = "Capacity CDF"
        Me.btnCapacityCDF.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1345, 73)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 13)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "Capacity bit/s"
        '
        'txtCapacity
        '
        Me.txtCapacity.Location = New System.Drawing.Point(1346, 89)
        Me.txtCapacity.Multiline = True
        Me.txtCapacity.Name = "txtCapacity"
        Me.txtCapacity.ReadOnly = True
        Me.txtCapacity.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCapacity.Size = New System.Drawing.Size(184, 456)
        Me.txtCapacity.TabIndex = 32
        Me.txtCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnPathLossCDF
        '
        Me.btnPathLossCDF.Location = New System.Drawing.Point(12, 551)
        Me.btnPathLossCDF.Name = "btnPathLossCDF"
        Me.btnPathLossCDF.Size = New System.Drawing.Size(184, 23)
        Me.btnPathLossCDF.TabIndex = 37
        Me.btnPathLossCDF.Text = "Path Loss CDF"
        Me.btnPathLossCDF.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "FSPL"
        '
        'txtFSPL
        '
        Me.txtFSPL.Location = New System.Drawing.Point(12, 89)
        Me.txtFSPL.Multiline = True
        Me.txtFSPL.Name = "txtFSPL"
        Me.txtFSPL.ReadOnly = True
        Me.txtFSPL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFSPL.Size = New System.Drawing.Size(184, 456)
        Me.txtFSPL.TabIndex = 35
        Me.txtFSPL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'FormDebug
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1584, 662)
        Me.Controls.Add(Me.btnPathLossCDF)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtFSPL)
        Me.Controls.Add(Me.btnCapacityCDF)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtCapacity)
        Me.Controls.Add(Me.btnSINRCDF)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtSINR)
        Me.Controls.Add(Me.btnSIRCDF)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtSIR)
        Me.Controls.Add(Me.btnSNRCDF)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtSNR)
        Me.Controls.Add(Me.btnInteferencePowerCDF)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtInterferencePower)
        Me.Controls.Add(Me.btnNoisePowerCDF)
        Me.Controls.Add(Me.lblNoise)
        Me.Controls.Add(Me.txtNoisePower)
        Me.Controls.Add(Me.btnSignalPowerCDF)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtSignalPower)
        Me.Controls.Add(Me.btnPopulate)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormDebug"
        Me.Text = "Graphs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPopulate As System.Windows.Forms.Button
    Friend WithEvents txtSignalPower As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSignalPowerCDF As System.Windows.Forms.Button
    Friend WithEvents btnNoisePowerCDF As System.Windows.Forms.Button
    Friend WithEvents lblNoise As System.Windows.Forms.Label
    Friend WithEvents txtNoisePower As System.Windows.Forms.TextBox
    Friend WithEvents btnInteferencePowerCDF As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtInterferencePower As System.Windows.Forms.TextBox
    Friend WithEvents btnSNRCDF As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSNR As System.Windows.Forms.TextBox
    Friend WithEvents btnSIRCDF As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSIR As System.Windows.Forms.TextBox
    Friend WithEvents btnSINRCDF As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSINR As System.Windows.Forms.TextBox
    Friend WithEvents btnCapacityCDF As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCapacity As System.Windows.Forms.TextBox
    Friend WithEvents btnPathLossCDF As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFSPL As System.Windows.Forms.TextBox
End Class
