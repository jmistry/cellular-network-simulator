﻿Imports System.Math

Public Class FormMain
    Public NETWORK As New Network
    Public BASESTATIONS() As Basestation
    Public USERS() As User
    Public HOTSPOTS(-1) As Hotspot

    'Global constants
    Public speed_of_light As Double = 299792458 'm/s
    Public Boltzmann_const As Decimal = (1.3806488 * Pow(10, -23))
    Public place_hotspot_mode As Boolean = False

    'Graphing Variables
    Public User_Capacity() As Double

    'Create a single instance of the Network class inside FormMain.vb
    Private Sub btnGenerateNetwork_Click(sender As Object, e As EventArgs) _
        Handles btnGenerateNetwork.Click
        'Setup network parameters
        NETWORK.size.X = txtNetworkSize.Text
        NETWORK.size.Y = txtNetworkSize.Text
        NETWORK.CalculateScalingFactor()
        NETWORK.basestation_separation = txtBasestationSeparation.Text
        NETWORK.carrier_frequency = txtNWcarrierFrequency.Text * Pow(10, 6)
        NETWORK.bandwidth = txtNWbandwidth.Text * Pow(10, 6)

        'Clear the drawing form at start
        txtAppStatus.AppendText("Clearing drawing area... ")
        FormDrawing.PanelNetworkDrawing.Invalidate()
        txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Size & show network
        FormDrawing.Show()
        txtAppStatus.AppendText("Creating Network... ")
        NETWORK.CalculateNumBasestations()
        ReDim BASESTATIONS(NETWORK.basestation_count - 1) 'resize the array to fit the number of basestations
        FormDrawing.PanelNetworkDrawing.Width = NETWORK.size.X * NETWORK.scaling_factor
        FormDrawing.PanelNetworkDrawing.Height = NETWORK.size.Y * NETWORK.scaling_factor
        txtAppStatus.AppendText("done!" & Environment.NewLine)
        txtAppStatus.AppendText("Network area: " & NETWORK.size.X / 1000 * NETWORK.size.Y / 1000 & " km²" & Environment.NewLine)
        txtAppStatus.AppendText("Basestations: " & NETWORK.basestation_count & Environment.NewLine)

        'Enable the "Generate Basestations" button
        btnGenerateBasestations.Enabled = True

        'Disable the "Generate Network" button
        btnGenerateNetwork.Enabled = False
    End Sub

    'Apply clustering frequency reuse to basestations
    Private Sub basestationsClusteringFreqReuse()
        'Determine frequency bands
        If cmbClusterGroupSizeN.Text = "" Then cmbClusterGroupSizeN.Text = 1
        NETWORK.clustering_cluster_size_N = CInt(cmbClusterGroupSizeN.Text)
        ReDim NETWORK.clustering_frequency_bands(NETWORK.clustering_cluster_size_N - 1)
        Dim band_width As Double = NETWORK.bandwidth / NETWORK.clustering_cluster_size_N
        For band_num As Integer = 0 To NETWORK.clustering_cluster_size_N - 1
            NETWORK.clustering_frequency_bands(band_num) = _
                New Bandwidth(NETWORK.carrier_frequency + band_width * (band_num + 1),
                              NETWORK.carrier_frequency + band_width * band_num)
        Next

        'Assign a frequency band to each basestation (Depends on cluster size N)
        Select Case NETWORK.clustering_cluster_size_N
            Case 1
                For Each Basestation In BASESTATIONS
                    'Assign same band to all users
                    Basestation.band = NETWORK.clustering_frequency_bands(0)
                    Basestation.clustering_group = 0
                Next
            Case 3 'cluster size = 3
                'iterate through columns
                For column As Integer = 0 To NETWORK.basestation_count_x
                    Dim band_selecter As Integer = -1 'starting value, -1 is invalid
                    If column Mod 2 = 0 Then 'even column
                        band_selecter = 0 'starting value
                    ElseIf column Mod 2 <> 0 Then 'odd column
                        band_selecter = 2 'starting value
                    End If

                    'iterate through rows, with constant colums
                    For row As Integer = 0 To NETWORK.basestation_count_y
                        For Each Basestation In BASESTATIONS
                            If Basestation.location.X = column And Basestation.location.Y = row Then
                                Basestation.clustering_group = band_selecter
                                Basestation.band = NETWORK.clustering_frequency_bands(band_selecter)
                                band_selecter += 1
                                If band_selecter = NETWORK.clustering_cluster_size_N Then band_selecter = 0
                            End If
                        Next
                    Next
                Next

            Case 4 'cluster size = 4
                'iterate through columns
                For column As Integer = 0 To NETWORK.basestation_count_x
                    Dim band_selecter As Integer = -1 'starting value, -1 is invalid
                    If column Mod 2 = 0 Then 'even column
                        band_selecter = 0 'starting value

                        'iterate through rows, with constant colums
                        For row As Integer = 0 To NETWORK.basestation_count_y
                            For Each Basestation In BASESTATIONS
                                If Basestation.location.X = column And Basestation.location.Y = row Then
                                    Basestation.clustering_group = band_selecter
                                    Basestation.band = NETWORK.clustering_frequency_bands(band_selecter)
                                    band_selecter += 1
                                    'max value for this column reached, reset to 0
                                    If band_selecter = 2 Then band_selecter = 0
                                End If
                            Next
                        Next
                    ElseIf column Mod 2 <> 0 Then 'odd column
                        band_selecter = 2 'starting value

                        'iterate through rows, with constant colums
                        For row As Integer = 0 To NETWORK.basestation_count_y
                            For Each Basestation In BASESTATIONS
                                If Basestation.location.X = column And Basestation.location.Y = row Then
                                    Basestation.clustering_group = band_selecter
                                    Basestation.band = NETWORK.clustering_frequency_bands(band_selecter)
                                    band_selecter += 1
                                    'max value for this column reached, reset to 2
                                    If band_selecter = 4 Then band_selecter = 2
                                End If
                            Next
                        Next
                    End If
                Next

            Case 7 'cluster size = 7
                'iterate through columns
                For column As Integer = 0 To NETWORK.basestation_count_x
                    Dim band_selecter As Integer = Tools.clustering_starting_band_7(column) 'starting value                       
                    'iterate through rows, with constant colums
                    For row As Integer = 0 To NETWORK.basestation_count_y
                        For Each Basestation In BASESTATIONS
                            If Basestation.location.X = column And Basestation.location.Y = row Then
                                Basestation.clustering_group = band_selecter
                                Basestation.band = NETWORK.clustering_frequency_bands(band_selecter)
                                band_selecter += 1
                                If band_selecter = 7 Then
                                    band_selecter = 0 'max value for this column reached, reset to 0
                                End If

                            End If
                        Next
                    Next
                Next
        End Select
    End Sub

    'Apply sectoring frequency reuse to basestations
    Private Sub basestationsSectoringFreqReuse()
        Dim sector_count As Integer = cmbSectors.Items(cmbSectors.SelectedIndex).ToString

        'Split network bandwidth into sub-bands for each sector
        ReDim NETWORK.sectoring_frequency_bands(sector_count - 1)
        Dim band_width As Double = NETWORK.bandwidth / sector_count
        For band_num As Integer = 0 To sector_count - 1
            NETWORK.sectoring_frequency_bands(band_num) = _
                New Bandwidth(NETWORK.carrier_frequency + band_width * (band_num + 1),
                              NETWORK.carrier_frequency + band_width * band_num)
        Next

        'Apply Full Frequency Reuse if selected
        If Me.chkSectoringFullReuse.Checked = True Then
            For Each Bandwidth In NETWORK.sectoring_frequency_bands
                Bandwidth.lower_frequency = NETWORK.carrier_frequency
                Bandwidth.upper_frequency = NETWORK.carrier_frequency + NETWORK.bandwidth
                Bandwidth.bandwidth = NETWORK.bandwidth
            Next
        End If

        'Assign bandwidths to basestations
        For column As Integer = 0 To NETWORK.basestation_count_x
            Dim band_selecter As Integer = -1 'starting value, -1 is invalid
            If column Mod 2 = 0 Then 'even column
                band_selecter = 0 'starting value
            ElseIf column Mod 2 <> 0 Then 'odd column
                band_selecter = 2 'starting value
            End If

            'Iterate through rows, with constant colums
            For row As Integer = 0 To NETWORK.basestation_count_y
                For Each Basestation In BASESTATIONS
                    ReDim Basestation.sectors(sector_count - 1)
                    If Basestation.location.X = column And Basestation.location.Y = row Then
                        Basestation.sectoring_group = band_selecter
                        band_selecter += 1

                        If band_selecter = NETWORK.sectoring_frequency_bands.Count Then
                            band_selecter = 0
                        End If

                    End If
                Next
            Next
        Next

        For Each Basestation In BASESTATIONS
            Basestation.determineSectors()
        Next

    End Sub

    'Create several instances of the Basestation class inside FormMain.vb
    Private Sub btnGenerateBasestations_Click(sender As Object, e As EventArgs) _
        Handles btnGenerateBasestations.Click

        txtAppStatus.AppendText(Environment.NewLine & "Generating Basestations... ")

        'Define Cells
        Dim ID As Integer = 0
        For x As Integer = 0 To NETWORK.basestation_count_x - 1
            For y As Integer = 0 To NETWORK.basestation_count_y - 1
                BASESTATIONS(ID) = New Basestation(ID, New Point(x, y),
                                                   txtBasestationSeparation.Text / 2,
                                                   txtBStxPower.Text,
                                                   txtBSheight.Text)
                'Determine cells of interest and shade them
                If (x > NETWORK.basestation_count_x * 0.1) And
                    (x < NETWORK.basestation_count_x * 0.7) And
                    (y > NETWORK.basestation_count_y * 0.1) And
                    (y < NETWORK.basestation_count_y * 0.7) Then
                    'These are the cells that we shall analyse, as they suffer from 
                    'full intercell interference
                    BASESTATIONS(ID).interesting = True
                End If
                'Increment ID value
                ID += 1
            Next
        Next

        'Perform bandwidth allocation based on frequency reuse scheme
        Select Case cmbFreqReuse.SelectedIndex
            Case 0 'None
                For basestation_ID As Integer = 0 To NETWORK.basestation_count - 1
                    Dim band_width As Double = NETWORK.bandwidth / NETWORK.basestation_count
                    BASESTATIONS(basestation_ID).band = _
                        New Bandwidth(NETWORK.carrier_frequency + band_width * (basestation_ID + 1),
                                      NETWORK.carrier_frequency + band_width * basestation_ID)
                    BASESTATIONS(basestation_ID).clustering_group = 0
                Next
            Case 1 'Clustering
                basestationsClusteringFreqReuse()
                Me.NETWORK.clustering = True
            Case 2 'Sectoring
                basestationsSectoringFreqReuse()
                Me.NETWORK.sectoring = True
        End Select

        'Draw basestations on form
        For Each Basestation In BASESTATIONS
            Basestation.draw()
        Next

        txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Enable the "Generate Users" button
        btnGenerateUsers.Enabled = True
        btnPlaceHotspot.Enabled = True

        'Update network stats form - basestation count
        FormNetworkStats.UpdateBasestationCount()

        'Disable generate basestations button
        btnGenerateBasestations.Enabled = False
    End Sub

    'Create multiple instances of the User class inside FormMain.vb
    Private Sub btnGenerateUsers_Click(sender As Object, e As EventArgs) _
        Handles btnGenerateUsers.Click
        'Disable the place hotspot button
        btnPlaceHotspot.Enabled = False

        'Save users settings
        NETWORK.user_count = Pow(numUsers.Value, 2)
        ReDim USERS(NETWORK.user_count - 1) 'resize the array to fit the number of basestations

        'Define local variable(s)
        Dim user_ID As Integer = 0 'start IDs from zero
        Dim cell_ID_lower As Integer = BASESTATIONS(BASESTATIONS.Count - 1).ID 'will store cell ID of first interesting cell. intialise to last cell ID
        Dim cell_ID_upper As Integer = 0 'will store cell ID of last interesting cell. initialise to first cell ID
        Dim user_area_scaled As Rectangle
        Dim user_area_top_left As Point
        Dim user_area_bottom_right As Point

        'Find IDs of interesting basestations
        For Each Basestation In BASESTATIONS
            If Basestation.interesting = True Then
                If Basestation.ID < cell_ID_lower Then
                    cell_ID_lower = Basestation.ID
                ElseIf Basestation.ID > cell_ID_upper Then
                    cell_ID_upper = Basestation.ID
                End If
            End If
        Next

        'Define area over which to distribute users
        txtAppStatus.AppendText(Environment.NewLine & "Defining user distribution area... ")
        user_area_top_left.X = BASESTATIONS(cell_ID_lower).hexagon_verticies(3).X
        user_area_top_left.Y = BASESTATIONS(cell_ID_lower).hexagon_verticies(4).Y
        user_area_bottom_right.X = BASESTATIONS(cell_ID_upper).hexagon_verticies(0).X
        user_area_bottom_right.Y = BASESTATIONS(cell_ID_upper).hexagon_verticies(1).Y
        NETWORK.user_area = New Rectangle(user_area_top_left.X,
                                          user_area_top_left.Y,
                                          user_area_bottom_right.Y - user_area_top_left.Y,
                                          user_area_bottom_right.Y - user_area_top_left.Y)

        'Move the users area so that it's central over the cellular network
        NETWORK.user_area.X -= (NETWORK.user_area.X + NETWORK.user_area.Width - BASESTATIONS(cell_ID_upper).hexagon_verticies(0).X) / 2

        'Scale user area for drawing
        user_area_scaled.X = NETWORK.user_area.X * NETWORK.scaling_factor
        user_area_scaled.Y = NETWORK.user_area.Y * NETWORK.scaling_factor
        user_area_scaled.Width = NETWORK.user_area.Width * NETWORK.scaling_factor
        user_area_scaled.Height = NETWORK.user_area.Height * NETWORK.scaling_factor

        'Draw rectagle on screen
        FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawRectangle(Pens.Black, user_area_scaled)
        txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Generate users
        txtAppStatus.AppendText("Generating users... ")
        If radioUDuniform.Checked = True Then
            'Uniformly distribute in network area
            Dim x_separation As Integer = NETWORK.user_area.Width / Sqrt(NETWORK.user_count)
            Dim y_separation As Integer = NETWORK.user_area.Height / Sqrt(NETWORK.user_count)
            For x As Integer = 0 To Sqrt(NETWORK.user_count) - 1
                For y As Integer = 0 To Sqrt(NETWORK.user_count) - 1
                    USERS(user_ID) = New User(user_ID,
                                              New Point(NETWORK.user_area.X + x * y_separation + ((NETWORK.user_area.Height / Sqrt(NETWORK.user_count)) / 2),
                                                        NETWORK.user_area.Y + y * y_separation + ((NETWORK.user_area.Height / Sqrt(NETWORK.user_count)) / 2)),
                                                    Me.txtUserHeight.Text)
                    user_ID += 1
                Next
            Next

        ElseIf radioUDrandom.Checked = True Then
            'Randomly distribute users in network area
            Dim x_position As Integer
            Dim y_position As Integer
            Randomize() 'initialize the random-number generator with a seed based on the system timer
            For i As Integer = 0 To NETWORK.user_count - 1
                x_position = NETWORK.user_area.X + Rnd() * NETWORK.user_area.Width
                y_position = NETWORK.user_area.Y + Rnd() * NETWORK.user_area.Height
                USERS(i) = New User(i, New Point(x_position, y_position), Me.txtUserHeight.Text)
            Next
        End If

        'Add hotspot users
        If Me.HOTSPOTS.Count <> 0 Then
            MsgBox("Number of hotspots: " & Me.HOTSPOTS.Count)

            For Each Hotspot In HOTSPOTS
                For hotspot_user As Integer = 0 To Hotspot.num_users
                    Dim ID As Integer = USERS.Count 'One more than the last user in USERS()
                    Dim user_position As Point

                    'Assign random position in hotspot
                    Randomize()
                    ' user_position.X = Hotspot.center_point(0).X - Hotspot.radius + Rnd() * Hotspot.radius * 2
                    ' user_position.Y = Hotspot.center_point(0).Y - Hotspot.radius + Rnd() * Hotspot.radius * 2
                    user_position.X = Hotspot.center_point(0).X + Rnd() * Hotspot.radius * Cos(2 * Math.PI * Rnd())
                    user_position.Y = Hotspot.center_point(0).Y + Rnd() * Hotspot.radius * Sin(2 * Math.PI * Rnd())


                    'Create new user
                    Dim new_User As New User(ID, user_position, Me.txtUserHeight.Text)

                    'Append new user to USERS()
                    ArrAppend(USERS, new_User)
                Next
            Next

            'Update network user count
            NETWORK.user_count = USERS.Count
        End If

        'Draw users on screen
        For user_ID = 0 To NETWORK.user_count - 1
            Dim scaled_location As Point
            scaled_location.X = USERS(user_ID).center_point.X * NETWORK.scaling_factor
            scaled_location.Y = USERS(user_ID).center_point.Y * NETWORK.scaling_factor
            FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawRectangle(Pens.Red, New Rectangle(scaled_location, New Size(1, 1)))
        Next
        txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Determine serving basestation
        For Each User In USERS
            User.DetermineServingBasestation()
        Next

        'Determine if user is interesting, grey out the uninteresting ones
        For Each User In USERS
            User.DetermineIfInteresting(BASESTATIONS)
            If User.interesting = False Then
                Dim scaled_location As Point
                scaled_location.X = User.center_point.X * NETWORK.scaling_factor
                scaled_location.Y = User.center_point.Y * NETWORK.scaling_factor
                FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawRectangle(Pens.Black, New Rectangle(scaled_location, New Size(1, 1)))
            End If
        Next

        'Determine user bandwidth
        For Each Basestation In BASESTATIONS
            Basestation.assignSubBands()
        Next

        'Calculate users' recieved signal and interference power
        txtAppStatus.AppendText("Calculating users' recieved power... ")
        For Each User In USERS
            If radioPL_FSPL.Checked = True Then
                User.CalculateSignalPower("FSPL")
                User.CalculateInterferencePower("FSPL")
            ElseIf radioPL_COSTHATA.Checked = True Then
                User.CalculateSignalPower("COSTHATA")
                User.CalculateInterferencePower("COSTHATA")
            ElseIf radioPL_3GPP.Checked = True Then
                User.CalculateSignalPower("3GPP")
                User.CalculateInterferencePower("3GPP")
            End If
        Next
        txtAppStatus.AppendText("done!" & Environment.NewLine)

        'Calculate noise power and capacity
        txtAppStatus.AppendText("Calculating users' Capacity... ")
        For Each User In USERS
            If User.interesting = True Then 'only calculate for users in network
                User.CalculateNoisePower(293) '293 K = 20 Celcius
                User.CalculateCapacity()
            End If
        Next
        txtAppStatus.AppendText("done!" & Environment.NewLine & Environment.NewLine)

        'Update network stats form - user count
        FormNetworkStats.UpdateUserCount()

        'Show results form
        FormGraphs.Show()

        'Calculate Network Stats
        calculateNetworkStatistics()

        'Disable Generate Users button
        btnGenerateUsers.Enabled = False
    End Sub

    'Calculate network statistics
    Public Sub calculateNetworkStatistics()
        txtAppStatus.AppendText("Calculating Network Stats... ")
        NETWORK.CalculateNetworkStats(BASESTATIONS, USERS)
        txtAppStatus.AppendText("done!" & Environment.NewLine)
    End Sub

    'UI functions
    Private Sub FormMain_Load(sender As Object, e As EventArgs) _
        Handles MyBase.Load
        ' Align to center of screen
        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2

        ' Move to left
        Me.Left = 50

        cmbFreqReuse.SelectedIndex = 0
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub RestartToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles RestartToolStripMenuItem.Click
        Application.Restart()
    End Sub

    Private Sub AboutCellularNetworkSimulatorToolStripMenuItem_Click(sender As Object, e As EventArgs) _
        Handles AboutCellularNetworkSimulatorToolStripMenuItem.Click
        FormAboutBox.Show()
    End Sub

    Private Sub cmbFreqReuse_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles cmbFreqReuse.SelectedIndexChanged, cmbPropagationEnv.SelectedIndexChanged
        If cmbFreqReuse.SelectedIndex = 0 Then
            cmbClusterGroupSizeN.Enabled = False
            cmbSectors.Enabled = False
            chkSectoringFullReuse.Enabled = False
            chkSectoringFullReuse.Checked = False
        ElseIf cmbFreqReuse.SelectedIndex = 1 Then
            cmbClusterGroupSizeN.Enabled = True
            cmbClusterGroupSizeN.SelectedIndex = 0
            cmbSectors.SelectedIndex = -1
            cmbSectors.Enabled = False
            chkSectoringFullReuse.Enabled = False
            chkSectoringFullReuse.Checked = False
        ElseIf cmbFreqReuse.SelectedIndex = 2 Then
            cmbSectors.Enabled = True
            cmbSectors.SelectedIndex = 0
            cmbClusterGroupSizeN.SelectedIndex = -1
            cmbClusterGroupSizeN.Enabled = False
            chkSectoringFullReuse.Enabled = True
            chkSectoringFullReuse.Checked = False
        End If

    End Sub

    Private Sub btnPlaceHotspot_Click(sender As Object, e As EventArgs) Handles btnPlaceHotspot.Click
        'Bring Drawing form into focus
        FormDrawing.Focus()

        'Set mouse position at center of drawing form
        Dim new_position As New Point(FormDrawing.Location.X + FormDrawing.Width / 2,
                                      FormDrawing.Location.Y + FormDrawing.Height / 2)
        Windows.Forms.Cursor.Position = new_position
        FormDrawing.Cursor = Cursors.Hand

        'Turn on place hotspot mode
        Me.place_hotspot_mode = True
    End Sub

    Private Sub radioPL_COSTHATA_CheckedChanged(sender As Object, e As EventArgs) Handles radioPL_COSTHATA.CheckedChanged
        If radioPL_COSTHATA.Checked = True Then
            cmbPropagationEnv.Enabled = True
            cmbPropagationEnv.SelectedIndex = 0

        Else
            cmbPropagationEnv.Enabled = False
            cmbPropagationEnv.SelectedIndex = -1
        End If
    End Sub
End Class
