﻿Imports System.Math

Public Class FormSectorDetails

    Private cell As Basestation
    Private sector As Sector
    Private sector_num As Integer

    Public Sub New(ByVal this_basestation As Basestation, ByVal this_sector As Sector)
        InitializeComponent()

        Me.cell = this_basestation
        Me.sector = this_sector

        sector_num = Me.sector.sectorNumber()
        Me.Text = "Sector: " & sector_num
        Me.txtSectorNumber.Text = sector_num
        Me.txtID.Text = this_basestation.ID
        Me.txtLocation.Text = Me.cell.center_point(0).X & ", " & Me.cell.center_point(0).Y
        Me.txtInteresting.Text = Me.cell.interesting
        Me.txtUsers.Text = Me.cell.users_IDs.Length
        Me.txtBandwidth.Text = Tools.eng(Me.cell.sectors(sector_num).band.bandwidth)
        Me.txtLowerFrequency.Text = Tools.eng(Me.cell.sectors(sector_num).band.lower_frequency)
        Me.txtUpperFrequency.Text = Tools.eng(Me.cell.sectors(sector_num).band.upper_frequency)
        For Each UserID In sector.users_IDs
            Me.txtUserList.AppendText(UserID & ", ")
        Next
    End Sub

    Private Sub btnLookupUser_Click(sender As Object, e As EventArgs) Handles btnLookupUser.Click
        Dim selected_user As New User(0, New Point(0, 0), 0)
        selected_user = FormMain.USERS(txtUserID.Text)

        Dim details_form As New FormUserDetails(selected_user)
        details_form.Show()

    End Sub

End Class