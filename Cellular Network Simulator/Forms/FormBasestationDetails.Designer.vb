﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBasestationDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBasestationDetails))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.txtCellRadius = New System.Windows.Forms.TextBox()
        Me.txtHorizontalSectors = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUsers = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtBandwidth = New System.Windows.Forms.TextBox()
        Me.txtUpperFrequency = New System.Windows.Forms.TextBox()
        Me.txtLowerFrequency = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtUserList = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtUserID = New System.Windows.Forms.TextBox()
        Me.btnLookupUser = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtInteresting = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Basestation ID:"
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(172, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 20)
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(172, 38)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(100, 20)
        Me.txtLocation.TabIndex = 2
        Me.txtLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCellRadius
        '
        Me.txtCellRadius.Location = New System.Drawing.Point(172, 90)
        Me.txtCellRadius.Name = "txtCellRadius"
        Me.txtCellRadius.Size = New System.Drawing.Size(100, 20)
        Me.txtCellRadius.TabIndex = 3
        Me.txtCellRadius.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHorizontalSectors
        '
        Me.txtHorizontalSectors.Location = New System.Drawing.Point(172, 116)
        Me.txtHorizontalSectors.Name = "txtHorizontalSectors"
        Me.txtHorizontalSectors.Size = New System.Drawing.Size(100, 20)
        Me.txtHorizontalSectors.TabIndex = 4
        Me.txtHorizontalSectors.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Cell Radius (m):"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Horizontal Sectors:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Location x,y (m):"
        '
        'txtUsers
        '
        Me.txtUsers.Location = New System.Drawing.Point(172, 142)
        Me.txtUsers.Name = "txtUsers"
        Me.txtUsers.Size = New System.Drawing.Size(100, 20)
        Me.txtUsers.TabIndex = 8
        Me.txtUsers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 145)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Users:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 197)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Bandwidth (Hz):"
        '
        'txtBandwidth
        '
        Me.txtBandwidth.Location = New System.Drawing.Point(172, 194)
        Me.txtBandwidth.Name = "txtBandwidth"
        Me.txtBandwidth.Size = New System.Drawing.Size(100, 20)
        Me.txtBandwidth.TabIndex = 8
        Me.txtBandwidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUpperFrequency
        '
        Me.txtUpperFrequency.Location = New System.Drawing.Point(172, 220)
        Me.txtUpperFrequency.Name = "txtUpperFrequency"
        Me.txtUpperFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtUpperFrequency.TabIndex = 8
        Me.txtUpperFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLowerFrequency
        '
        Me.txtLowerFrequency.Location = New System.Drawing.Point(172, 168)
        Me.txtLowerFrequency.Name = "txtLowerFrequency"
        Me.txtLowerFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtLowerFrequency.TabIndex = 8
        Me.txtLowerFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 223)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Upper Frequency (Hz):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 171)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(114, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Lower Frequency (Hz):"
        '
        'txtUserList
        '
        Me.txtUserList.Location = New System.Drawing.Point(15, 264)
        Me.txtUserList.Multiline = True
        Me.txtUserList.Name = "txtUserList"
        Me.txtUserList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtUserList.Size = New System.Drawing.Size(257, 103)
        Me.txtUserList.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 248)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "User List"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 401)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(265, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "CTRL + Click on network diagram to show user details."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 379)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "User ID:"
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(170, 376)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(61, 20)
        Me.txtUserID.TabIndex = 12
        Me.txtUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnLookupUser
        '
        Me.btnLookupUser.Location = New System.Drawing.Point(237, 373)
        Me.btnLookupUser.Name = "btnLookupUser"
        Me.btnLookupUser.Size = New System.Drawing.Size(35, 25)
        Me.btnLookupUser.TabIndex = 13
        Me.btnLookupUser.Text = "Info"
        Me.btnLookupUser.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 67)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "In simulation:"
        '
        'txtInteresting
        '
        Me.txtInteresting.Location = New System.Drawing.Point(172, 64)
        Me.txtInteresting.Name = "txtInteresting"
        Me.txtInteresting.Size = New System.Drawing.Size(100, 20)
        Me.txtInteresting.TabIndex = 8
        Me.txtInteresting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'FormBasestationDetails
        '
        Me.AcceptButton = Me.btnLookupUser
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 423)
        Me.Controls.Add(Me.btnLookupUser)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtUserList)
        Me.Controls.Add(Me.txtInteresting)
        Me.Controls.Add(Me.txtLowerFrequency)
        Me.Controls.Add(Me.txtUpperFrequency)
        Me.Controls.Add(Me.txtBandwidth)
        Me.Controls.Add(Me.txtUsers)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtHorizontalSectors)
        Me.Controls.Add(Me.txtCellRadius)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormBasestationDetails"
        Me.Text = "Basestation ID: "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtCellRadius As System.Windows.Forms.TextBox
    Friend WithEvents txtHorizontalSectors As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUsers As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBandwidth As System.Windows.Forms.TextBox
    Friend WithEvents txtUpperFrequency As System.Windows.Forms.TextBox
    Friend WithEvents txtLowerFrequency As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtUserList As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents btnLookupUser As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtInteresting As System.Windows.Forms.TextBox
End Class
