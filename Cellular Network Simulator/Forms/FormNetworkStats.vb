﻿Public Class FormNetworkStats

    'Display the number of users in the sumualtion
    Public Sub UpdateUserCount()
        Dim interesting_users As Integer = 0
        For Each User In FormMain.USERS
            If User.interesting = True Then
                interesting_users += 1
            End If
        Next
        lblUsersInSimCount.Text = interesting_users & " / " & FormMain.NETWORK.user_count
    End Sub

    'Display the number of basestations in the simulation
    Public Sub UpdateBasestationCount()
        Dim interesting_basestations As Integer = 0
        For Each Basestation In FormMain.BASESTATIONS
            If Basestation.interesting = True Then
                interesting_basestations += 1
            End If
        Next
        lblBasestationsInSimCount.Text = interesting_basestations & " / " & FormMain.NETWORK.basestation_count
    End Sub

    'Display the network capacity
    Public Sub UpdateNetworkCapacity()
        lblNetworkCapacity.Text = Tools.eng(FormMain.NETWORK.capacity * 0.01, 2)
        lblSpectrumEfficiency.Text = Math.Round((FormMain.NETWORK.capacity * 0.01 / FormMain.NETWORK.bandwidth), 2)
    End Sub

    'Display user capacity stats
    Public Sub UpdateUserCapacityStats()
        Dim peak As Double = -1
        Dim mean As Double = -1
        Dim sum As Double = 0
        Dim std_dev As Double = -1
        Dim median As Double = -1
        Dim capacity_arr(-1) As Double

        For Each User In FormMain.USERS
            If User.interesting = True Then
                ArrayExtensions.ArrAppend(capacity_arr, User.capacity)
                sum += User.capacity
                If User.capacity > peak Then
                    peak = User.capacity
                End If
            End If
        Next

        'Mean
        mean = sum / capacity_arr.Count

        'Median
        Array.Sort(capacity_arr)
        If capacity_arr.Count Mod 2 = 1 Then
            median = capacity_arr(capacity_arr.Count / 2 + 1)
        Else
            median = (capacity_arr(capacity_arr.Count / 2) + capacity_arr(capacity_arr.Count / 2 + 1)) / 2
        End If

        'Standard Deviation
        std_dev = Tools.StandardDeviation(capacity_arr)

        Me.lblUserCapacityMean.Text = Tools.eng(mean, 2)
        Me.lblUserCapacityPeak.Text = Tools.eng(peak, 2)
        Me.lblUserCapacityMedian.Text = Tools.eng(median, 2)
        Me.lblUserCapacityStdDev.Text = Tools.eng(std_dev, 2)
    End Sub

    'Display the network outage probability
    Public Sub UpdateOutageProbability()
        lblOutageProbability.Text = Math.Round(FormMain.NETWORK.outage_probability, 2)
    End Sub

    'Refresh all stats
    Public Sub UpdateAllStats()
        UpdateUserCount()
        UpdateBasestationCount()
        UpdateNetworkCapacity()
        UpdateUserCapacityStats()
        UpdateOutageProbability()
    End Sub

    'UI functions
    Private Sub FormNetworkStats_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = FormDrawing.Location
        Me.Top = FormDrawing.Top + FormDrawing.Height
    End Sub

End Class