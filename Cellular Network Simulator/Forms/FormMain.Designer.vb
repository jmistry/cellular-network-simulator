﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.RestartToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutCellularNetworkSimulatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnGenerateNetwork = New System.Windows.Forms.Button()
        Me.txtBasestationSeparation = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNetworkSize = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnGenerateBasestations = New System.Windows.Forms.Button()
        Me.txtBStxPower = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.numUsers = New System.Windows.Forms.NumericUpDown()
        Me.btnGenerateUsers = New System.Windows.Forms.Button()
        Me.txtAppStatus = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbClusterGroupSizeN = New System.Windows.Forms.ComboBox()
        Me.cmbSectors = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNWbandwidth = New System.Windows.Forms.TextBox()
        Me.txtNWcarrierFrequency = New System.Windows.Forms.TextBox()
        Me.cmbPropagationEnv = New System.Windows.Forms.ComboBox()
        Me.cmbFreqReuse = New System.Windows.Forms.ComboBox()
        Me.radioPL_3GPP = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.radioPL_COSTHATA = New System.Windows.Forms.RadioButton()
        Me.radioPL_FSPL = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkSectoringFullReuse = New System.Windows.Forms.CheckBox()
        Me.txtBSheight = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtUserHeight = New System.Windows.Forms.TextBox()
        Me.radioUDrandom = New System.Windows.Forms.RadioButton()
        Me.radioUDuniform = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMinThroughput = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnPlaceHotspot = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.numHotspotUsers = New System.Windows.Forms.NumericUpDown()
        Me.txtHotspotRadius = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.numUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.numHotspotUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(384, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenSettingsToolStripMenuItem, Me.SaveSettingsToolStripMenuItem, Me.ToolStripMenuItem1, Me.RestartToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'OpenSettingsToolStripMenuItem
        '
        Me.OpenSettingsToolStripMenuItem.Name = "OpenSettingsToolStripMenuItem"
        Me.OpenSettingsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenSettingsToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.OpenSettingsToolStripMenuItem.Text = "&Open Configuration"
        '
        'SaveSettingsToolStripMenuItem
        '
        Me.SaveSettingsToolStripMenuItem.Name = "SaveSettingsToolStripMenuItem"
        Me.SaveSettingsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveSettingsToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.SaveSettingsToolStripMenuItem.Text = "&Save Configuration"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(220, 6)
        '
        'RestartToolStripMenuItem
        '
        Me.RestartToolStripMenuItem.Name = "RestartToolStripMenuItem"
        Me.RestartToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.RestartToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.RestartToolStripMenuItem.Text = "&Restart"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutCellularNetworkSimulatorToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'AboutCellularNetworkSimulatorToolStripMenuItem
        '
        Me.AboutCellularNetworkSimulatorToolStripMenuItem.Name = "AboutCellularNetworkSimulatorToolStripMenuItem"
        Me.AboutCellularNetworkSimulatorToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.AboutCellularNetworkSimulatorToolStripMenuItem.Text = "&About Cellular Network Simulator"
        '
        'btnGenerateNetwork
        '
        Me.btnGenerateNetwork.Location = New System.Drawing.Point(12, 579)
        Me.btnGenerateNetwork.Name = "btnGenerateNetwork"
        Me.btnGenerateNetwork.Size = New System.Drawing.Size(106, 23)
        Me.btnGenerateNetwork.TabIndex = 0
        Me.btnGenerateNetwork.Text = "Generate Network"
        Me.btnGenerateNetwork.UseVisualStyleBackColor = True
        '
        'txtBasestationSeparation
        '
        Me.txtBasestationSeparation.Location = New System.Drawing.Point(254, 45)
        Me.txtBasestationSeparation.Name = "txtBasestationSeparation"
        Me.txtBasestationSeparation.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtBasestationSeparation.Size = New System.Drawing.Size(100, 20)
        Me.txtBasestationSeparation.TabIndex = 3
        Me.txtBasestationSeparation.Text = "1000"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Basestation Separation (m)"
        '
        'txtNetworkSize
        '
        Me.txtNetworkSize.Location = New System.Drawing.Point(254, 19)
        Me.txtNetworkSize.Name = "txtNetworkSize"
        Me.txtNetworkSize.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtNetworkSize.Size = New System.Drawing.Size(100, 20)
        Me.txtNetworkSize.TabIndex = 1
        Me.txtNetworkSize.Text = "10000"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Network Size (m)"
        '
        'btnGenerateBasestations
        '
        Me.btnGenerateBasestations.Enabled = False
        Me.btnGenerateBasestations.Location = New System.Drawing.Point(124, 579)
        Me.btnGenerateBasestations.Name = "btnGenerateBasestations"
        Me.btnGenerateBasestations.Size = New System.Drawing.Size(135, 23)
        Me.btnGenerateBasestations.TabIndex = 1
        Me.btnGenerateBasestations.Text = "Generate Basestations"
        Me.btnGenerateBasestations.UseVisualStyleBackColor = True
        '
        'txtBStxPower
        '
        Me.txtBStxPower.Location = New System.Drawing.Point(254, 19)
        Me.txtBStxPower.Name = "txtBStxPower"
        Me.txtBStxPower.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtBStxPower.Size = New System.Drawing.Size(100, 20)
        Me.txtBStxPower.TabIndex = 7
        Me.txtBStxPower.Text = "46"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Transmit Power (dBm)"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "SQRT(Users)"
        '
        'numUsers
        '
        Me.numUsers.Location = New System.Drawing.Point(254, 19)
        Me.numUsers.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numUsers.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numUsers.Name = "numUsers"
        Me.numUsers.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.numUsers.Size = New System.Drawing.Size(100, 20)
        Me.numUsers.TabIndex = 1
        Me.numUsers.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'btnGenerateUsers
        '
        Me.btnGenerateUsers.Enabled = False
        Me.btnGenerateUsers.Location = New System.Drawing.Point(265, 579)
        Me.btnGenerateUsers.Name = "btnGenerateUsers"
        Me.btnGenerateUsers.Size = New System.Drawing.Size(107, 23)
        Me.btnGenerateUsers.TabIndex = 2
        Me.btnGenerateUsers.Text = "Generate Users"
        Me.btnGenerateUsers.UseVisualStyleBackColor = True
        '
        'txtAppStatus
        '
        Me.txtAppStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAppStatus.Location = New System.Drawing.Point(3, 16)
        Me.txtAppStatus.Multiline = True
        Me.txtAppStatus.Name = "txtAppStatus"
        Me.txtAppStatus.ReadOnly = True
        Me.txtAppStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAppStatus.Size = New System.Drawing.Size(354, 169)
        Me.txtAppStatus.TabIndex = 13
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtAppStatus)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 608)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(360, 188)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Status"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Cluster group size N"
        '
        'cmbClusterGroupSizeN
        '
        Me.cmbClusterGroupSizeN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClusterGroupSizeN.FormattingEnabled = True
        Me.cmbClusterGroupSizeN.Items.AddRange(New Object() {"1", "3", "4", "7"})
        Me.cmbClusterGroupSizeN.Location = New System.Drawing.Point(254, 71)
        Me.cmbClusterGroupSizeN.Name = "cmbClusterGroupSizeN"
        Me.cmbClusterGroupSizeN.Size = New System.Drawing.Size(100, 21)
        Me.cmbClusterGroupSizeN.TabIndex = 10
        '
        'cmbSectors
        '
        Me.cmbSectors.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSectors.FormattingEnabled = True
        Me.cmbSectors.Items.AddRange(New Object() {"3"})
        Me.cmbSectors.Location = New System.Drawing.Point(254, 98)
        Me.cmbSectors.Name = "cmbSectors"
        Me.cmbSectors.Size = New System.Drawing.Size(100, 21)
        Me.cmbSectors.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 101)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Horizontal Sectors"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNWbandwidth)
        Me.GroupBox1.Controls.Add(Me.txtNWcarrierFrequency)
        Me.GroupBox1.Controls.Add(Me.cmbPropagationEnv)
        Me.GroupBox1.Controls.Add(Me.cmbFreqReuse)
        Me.GroupBox1.Controls.Add(Me.radioPL_3GPP)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.radioPL_COSTHATA)
        Me.GroupBox1.Controls.Add(Me.radioPL_FSPL)
        Me.GroupBox1.Controls.Add(Me.txtBasestationSeparation)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtNetworkSize)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 205)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Network Settings"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 149)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(126, 13)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "Propagation Environment"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 100)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 13)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Bandwidth (MHz)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Carrier Frequency (MHz)"
        '
        'txtNWbandwidth
        '
        Me.txtNWbandwidth.Location = New System.Drawing.Point(254, 97)
        Me.txtNWbandwidth.Name = "txtNWbandwidth"
        Me.txtNWbandwidth.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtNWbandwidth.Size = New System.Drawing.Size(100, 20)
        Me.txtNWbandwidth.TabIndex = 16
        Me.txtNWbandwidth.Text = "10"
        '
        'txtNWcarrierFrequency
        '
        Me.txtNWcarrierFrequency.Location = New System.Drawing.Point(254, 71)
        Me.txtNWcarrierFrequency.Name = "txtNWcarrierFrequency"
        Me.txtNWcarrierFrequency.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtNWcarrierFrequency.Size = New System.Drawing.Size(100, 20)
        Me.txtNWcarrierFrequency.TabIndex = 16
        Me.txtNWcarrierFrequency.Text = "2000"
        '
        'cmbPropagationEnv
        '
        Me.cmbPropagationEnv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropagationEnv.Enabled = False
        Me.cmbPropagationEnv.FormattingEnabled = True
        Me.cmbPropagationEnv.Items.AddRange(New Object() {"Suburban", "Urban"})
        Me.cmbPropagationEnv.Location = New System.Drawing.Point(254, 146)
        Me.cmbPropagationEnv.Name = "cmbPropagationEnv"
        Me.cmbPropagationEnv.Size = New System.Drawing.Size(100, 21)
        Me.cmbPropagationEnv.TabIndex = 15
        '
        'cmbFreqReuse
        '
        Me.cmbFreqReuse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFreqReuse.FormattingEnabled = True
        Me.cmbFreqReuse.Items.AddRange(New Object() {"None", "Clustering", "Sectorisation"})
        Me.cmbFreqReuse.Location = New System.Drawing.Point(254, 173)
        Me.cmbFreqReuse.Name = "cmbFreqReuse"
        Me.cmbFreqReuse.Size = New System.Drawing.Size(100, 21)
        Me.cmbFreqReuse.TabIndex = 15
        '
        'radioPL_3GPP
        '
        Me.radioPL_3GPP.AutoSize = True
        Me.radioPL_3GPP.Location = New System.Drawing.Point(209, 123)
        Me.radioPL_3GPP.Name = "radioPL_3GPP"
        Me.radioPL_3GPP.Size = New System.Drawing.Size(53, 17)
        Me.radioPL_3GPP.TabIndex = 8
        Me.radioPL_3GPP.TabStop = True
        Me.radioPL_3GPP.Text = "3GPP"
        Me.radioPL_3GPP.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Frequency Reuse"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Path Loss Model"
        '
        'radioPL_COSTHATA
        '
        Me.radioPL_COSTHATA.AutoSize = True
        Me.radioPL_COSTHATA.Location = New System.Drawing.Point(268, 123)
        Me.radioPL_COSTHATA.Name = "radioPL_COSTHATA"
        Me.radioPL_COSTHATA.Size = New System.Drawing.Size(86, 17)
        Me.radioPL_COSTHATA.TabIndex = 7
        Me.radioPL_COSTHATA.Text = "COST HATA"
        Me.radioPL_COSTHATA.UseVisualStyleBackColor = True
        '
        'radioPL_FSPL
        '
        Me.radioPL_FSPL.AutoSize = True
        Me.radioPL_FSPL.Checked = True
        Me.radioPL_FSPL.Location = New System.Drawing.Point(152, 123)
        Me.radioPL_FSPL.Name = "radioPL_FSPL"
        Me.radioPL_FSPL.Size = New System.Drawing.Size(51, 17)
        Me.radioPL_FSPL.TabIndex = 6
        Me.radioPL_FSPL.TabStop = True
        Me.radioPL_FSPL.Text = "FSPL"
        Me.radioPL_FSPL.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSectoringFullReuse)
        Me.GroupBox2.Controls.Add(Me.txtBSheight)
        Me.GroupBox2.Controls.Add(Me.cmbSectors)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.cmbClusterGroupSizeN)
        Me.GroupBox2.Controls.Add(Me.txtBStxPower)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 238)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 129)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Basestations Settings"
        '
        'chkSectoringFullReuse
        '
        Me.chkSectoringFullReuse.AutoSize = True
        Me.chkSectoringFullReuse.Enabled = False
        Me.chkSectoringFullReuse.Location = New System.Drawing.Point(172, 101)
        Me.chkSectoringFullReuse.Name = "chkSectoringFullReuse"
        Me.chkSectoringFullReuse.Size = New System.Drawing.Size(76, 17)
        Me.chkSectoringFullReuse.TabIndex = 16
        Me.chkSectoringFullReuse.Text = "Full Reuse"
        Me.chkSectoringFullReuse.UseVisualStyleBackColor = True
        '
        'txtBSheight
        '
        Me.txtBSheight.Location = New System.Drawing.Point(254, 45)
        Me.txtBSheight.Name = "txtBSheight"
        Me.txtBSheight.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtBSheight.Size = New System.Drawing.Size(100, 20)
        Me.txtBSheight.TabIndex = 12
        Me.txtBSheight.Text = "30"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 48)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(124, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Height above ground (m)"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtUserHeight)
        Me.GroupBox3.Controls.Add(Me.radioUDrandom)
        Me.GroupBox3.Controls.Add(Me.radioUDuniform)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txtMinThroughput)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.numUsers)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 373)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(360, 120)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Users Settings"
        '
        'txtUserHeight
        '
        Me.txtUserHeight.Location = New System.Drawing.Point(254, 45)
        Me.txtUserHeight.Name = "txtUserHeight"
        Me.txtUserHeight.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtUserHeight.Size = New System.Drawing.Size(100, 20)
        Me.txtUserHeight.TabIndex = 12
        Me.txtUserHeight.Text = "1.65"
        '
        'radioUDrandom
        '
        Me.radioUDrandom.AutoSize = True
        Me.radioUDrandom.Checked = True
        Me.radioUDrandom.Location = New System.Drawing.Point(289, 97)
        Me.radioUDrandom.Name = "radioUDrandom"
        Me.radioUDrandom.Size = New System.Drawing.Size(65, 17)
        Me.radioUDrandom.TabIndex = 6
        Me.radioUDrandom.TabStop = True
        Me.radioUDrandom.Text = "Random"
        Me.radioUDrandom.UseVisualStyleBackColor = True
        '
        'radioUDuniform
        '
        Me.radioUDuniform.AutoSize = True
        Me.radioUDuniform.Location = New System.Drawing.Point(222, 97)
        Me.radioUDuniform.Name = "radioUDuniform"
        Me.radioUDuniform.Size = New System.Drawing.Size(61, 17)
        Me.radioUDuniform.TabIndex = 5
        Me.radioUDuniform.Text = "Uniform"
        Me.radioUDuniform.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 99)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Spatial Distribution"
        '
        'txtMinThroughput
        '
        Me.txtMinThroughput.Location = New System.Drawing.Point(254, 71)
        Me.txtMinThroughput.Name = "txtMinThroughput"
        Me.txtMinThroughput.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtMinThroughput.Size = New System.Drawing.Size(99, 20)
        Me.txtMinThroughput.TabIndex = 1
        Me.txtMinThroughput.Text = "0.1"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 74)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(139, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Outage Threshold (M bits/s)"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 48)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(124, 13)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "Height above ground (m)"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnPlaceHotspot)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.numHotspotUsers)
        Me.GroupBox5.Controls.Add(Me.txtHotspotRadius)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 499)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(360, 74)
        Me.GroupBox5.TabIndex = 10
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "User Hotspot"
        '
        'btnPlaceHotspot
        '
        Me.btnPlaceHotspot.Enabled = False
        Me.btnPlaceHotspot.Location = New System.Drawing.Point(268, 19)
        Me.btnPlaceHotspot.Name = "btnPlaceHotspot"
        Me.btnPlaceHotspot.Size = New System.Drawing.Size(86, 46)
        Me.btnPlaceHotspot.TabIndex = 4
        Me.btnPlaceHotspot.Text = "Place Hotspot"
        Me.btnPlaceHotspot.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 21)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(138, 13)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Number of Users In Hotspot"
        '
        'numHotspotUsers
        '
        Me.numHotspotUsers.Location = New System.Drawing.Point(162, 19)
        Me.numHotspotUsers.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.numHotspotUsers.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numHotspotUsers.Name = "numHotspotUsers"
        Me.numHotspotUsers.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.numHotspotUsers.Size = New System.Drawing.Size(100, 20)
        Me.numHotspotUsers.TabIndex = 1
        Me.numHotspotUsers.Value = New Decimal(New Integer() {500, 0, 0, 0})
        '
        'txtHotspotRadius
        '
        Me.txtHotspotRadius.Location = New System.Drawing.Point(162, 45)
        Me.txtHotspotRadius.Name = "txtHotspotRadius"
        Me.txtHotspotRadius.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtHotspotRadius.Size = New System.Drawing.Size(100, 20)
        Me.txtHotspotRadius.TabIndex = 1
        Me.txtHotspotRadius.Text = "500"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 48)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(97, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Hotspot Radius (m)"
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 808)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnGenerateBasestations)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnGenerateNetwork)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.btnGenerateUsers)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FormMain"
        Me.Text = "Cellular Network Simulator"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.numUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.numHotspotUsers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtNetworkSize As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateNetwork As System.Windows.Forms.Button
    Friend WithEvents txtBasestationSeparation As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateBasestations As System.Windows.Forms.Button
    Friend WithEvents txtBStxPower As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnGenerateUsers As System.Windows.Forms.Button
    Friend WithEvents numUsers As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAppStatus As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents OpenSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveSettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RestartToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radioPL_3GPP As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents radioPL_COSTHATA As System.Windows.Forms.RadioButton
    Friend WithEvents radioPL_FSPL As System.Windows.Forms.RadioButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbClusterGroupSizeN As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutCellularNetworkSimulatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radioUDrandom As System.Windows.Forms.RadioButton
    Friend WithEvents radioUDuniform As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbSectors As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbFreqReuse As System.Windows.Forms.ComboBox
    Friend WithEvents txtBSheight As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNWbandwidth As System.Windows.Forms.TextBox
    Friend WithEvents txtNWcarrierFrequency As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents numHotspotUsers As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtHotspotRadius As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnPlaceHotspot As System.Windows.Forms.Button
    Friend WithEvents txtMinThroughput As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbPropagationEnv As System.Windows.Forms.ComboBox
    Friend WithEvents txtUserHeight As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents chkSectoringFullReuse As System.Windows.Forms.CheckBox

End Class
