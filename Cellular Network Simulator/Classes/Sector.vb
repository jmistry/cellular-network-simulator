﻿''' <summary>
''' A class containg methods and attributes of a basestation sector
''' </summary>
''' <remarks></remarks>
Public Class Sector
    '///     Attributes     \\\'
    ''''''''''''''''''''''''''''
    'Public Variable Definitions
    Public band As Bandwidth
    Public verticies(3) As Point
    Public colour As Brush
    Public sub_bands() As Bandwidth 'sub-bands that can be assinged to users
    Public users_IDs(-1) As Integer

    '///       Methods      \\\'
    ''''''''''''''''''''''''''''
    'Constructor - used to make an instance of the class
    Public Sub New(ByVal bandwidth As Bandwidth,
                   ByVal hexagon_center_point As Point,
                   ByVal vertex1 As Point,
                   ByVal vertex2 As Point,
                   ByVal vertex3 As Point)
        band = bandwidth
        verticies(0) = hexagon_center_point
        verticies(1) = vertex1
        verticies(2) = vertex2
        verticies(3) = vertex3
        determineColour()
    End Sub

    'Determine sector colour based on which sector this is
    Private Sub determineColour()
        If Me.band.upper_frequency = FormMain.NETWORK.sectoring_frequency_bands(0).upper_frequency Then
            Me.colour = Brushes.LightBlue
        ElseIf Me.band.upper_frequency = FormMain.NETWORK.sectoring_frequency_bands(1).upper_frequency Then
            Me.colour = Brushes.LightGreen
        ElseIf Me.band.upper_frequency = FormMain.NETWORK.sectoring_frequency_bands(2).upper_frequency Then
            Me.colour = Brushes.LightCyan
        End If
    End Sub

    'Find users inside sector
    Public Sub findUsers()
        For Each User In FormMain.USERS
            If PointInPolygon(Me.verticies, User.center_point.X, User.center_point.Y) = True Then
                ArrAppend(Me.users_IDs, User.ID)
            End If
        Next
    End Sub

    'Determine sub-bands
    Public Sub determineSubBands()
        ReDim sub_bands(users_IDs.Length - 1) 'resize the array to support number of users in cell

        Dim band_width As Double = Me.band.bandwidth / Me.users_IDs.Length 'bandwidth to assign to each user

        For subBand_num As Integer = 0 To Me.sub_bands.Length - 1
            Me.sub_bands(subBand_num) = New Bandwidth(Me.band.lower_frequency + band_width * (subBand_num + 1),
                                                      Me.band.lower_frequency + band_width * subBand_num)
        Next
    End Sub

    'Assign sub-bands to users inside sector
    Public Sub assignSubBands()
        Dim i As Integer = 0

        For Each user_ID In Me.users_IDs
            FormMain.USERS(user_ID).bandwidth = Me.sub_bands(i)
            i += 1
        Next
    End Sub

    Function sectorNumber() As Integer
        Dim sector_num As Integer = -1

        For i = 0 To FormMain.NETWORK.sectoring_frequency_bands.Count - 1
            If Me.band.lower_frequency = FormMain.NETWORK.sectoring_frequency_bands(i).lower_frequency And
               Me.band.upper_frequency = FormMain.NETWORK.sectoring_frequency_bands(i).upper_frequency And
               Me.band.bandwidth = FormMain.NETWORK.sectoring_frequency_bands(i).bandwidth Then
                sector_num = i
            End If
        Next

        Return sector_num
    End Function

End Class