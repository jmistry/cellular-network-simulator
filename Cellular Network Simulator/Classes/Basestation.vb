﻿Imports System.Math

''' <summary>
''' A class defining attributes and methods associated with a Cellular Network Basestation
''' </summary>
''' <remarks></remarks>
Public Class Basestation
    '///     Attributes     \\\'
    ''''''''''''''''''''''''''''
    'Public Variable Definitions
    Public ID As Integer
    Public location As Point 'integer (x,y) location on grid of basestations. i.e. (0,0) (0,1)
    Public height As Double
    Public cell_radius As Double
    Public hexagon_verticies(5) As Point
    Public center_point(0) As Point 'location of center of hexagon in meters
    Public interesting As Boolean = False 'basestation considered interesting if within simulation boundary
    Public transmit_power_dBm As Double = 46 'dBm, log scale (40W)
    Public cell_capacity As Double = 0 'bits/sec
    Public clustering_group As Integer = -1 'which clustering group the cell belongs to
    Public sectors(0) As Sector
    Public sectoring_group As Integer = -1
    Public colour As Brush 'what colour the cell is filled with
    Public band As Bandwidth 'band of network bandwidth assigned to cell
    Public sub_bands() As Bandwidth 'sub-carriers that can be assinged to users
    Public users_IDs(-1) As Integer

    '///       Methods      \\\'
    ''''''''''''''''''''''''''''
    'Constructor - used to make an instance of the class
    Public Sub New(ByVal ID As Integer,
                   ByVal location As Point,
                   ByVal cell_radius As Double,
                   ByVal tx_power_dBm As Double,
                   ByVal height_above_ground As Double)
        Me.ID = ID
        Me.location = location
        Me.cell_radius = cell_radius
        Me.transmit_power_dBm = tx_power_dBm
        Me.height = height_above_ground
        Me.generateVerticies()
    End Sub

    'Generate verticies cell edge
    Public Sub generateVerticies()
        'Calculate size of hexagon
        Dim size_x As Integer = cell_radius
        Dim size_y As Integer = cell_radius

        'Counter variables for angular sweep
        Dim theta As Double = 0
        Dim dtheta As Double = 2 * Math.PI / 6

        'Generate points
        For i As Integer = 0 To hexagon_verticies.Length - 1
            hexagon_verticies(i).X = CInt(size_x + size_x * Cos(theta))
            hexagon_verticies(i).Y = CInt(size_y + size_y * Sin(theta)) - 67
            theta += dtheta
        Next i

        'Move cells to location
        Dim deltaX As Integer = (hexagon_verticies(0).X - hexagon_verticies(2).X)
        Dim deltaY As Integer = hexagon_verticies(1).Y
        For i As Integer = 0 To hexagon_verticies.Length - 1
            hexagon_verticies(i).X += location.X * deltaX
            hexagon_verticies(i).Y += location.Y * deltaY
        Next

        'Move cells down if in odd X location
        If location.X Mod 2 <> 0 Then
            Dim y_offset As Integer = hexagon_verticies(1).Y - hexagon_verticies(0).Y
            For i As Integer = 0 To hexagon_verticies.Length - 1
                hexagon_verticies(i).Y += y_offset
            Next
        End If

        'Calculate center point of hexagon
        Dim center_x As Integer = (hexagon_verticies(4).X + hexagon_verticies(5).X) / 2
        Dim center_y As Integer = (hexagon_verticies(4).Y + hexagon_verticies(2).Y) / 2
        center_point(0) = New Point(center_x, center_y)
    End Sub

    'Determine sub-bands
    Public Sub determineSubBands()
        If FormMain.NETWORK.sectoring = False Then
            ReDim sub_bands(users_IDs.Length - 1) 'resize the array to support number of users in cell

            Dim band_width As Double = Me.band.bandwidth / Me.users_IDs.Length 'bandwidth to assign to each user

            For subcarrier_num As Integer = 0 To Me.sub_bands.Length - 1
                Me.sub_bands(subcarrier_num) = New Bandwidth(Me.band.lower_frequency + band_width * (subcarrier_num + 1),
                                                             Me.band.lower_frequency + band_width * subcarrier_num)
            Next
        End If
    End Sub

    'Assign sub-bands to users
    Public Sub assignSubBands()
        If FormMain.NETWORK.sectoring = False Then
            determineSubBands()

            Dim i As Integer = 0

            For Each user_ID In users_IDs
                FormMain.USERS(user_ID).bandwidth = sub_bands(i)
                i += 1
            Next
        ElseIf FormMain.NETWORK.sectoring = True Then
            For Each Sector In Me.sectors
                Sector.findUsers()
                Sector.determineSubBands()
                Sector.assignSubBands()
            Next

        End If

    End Sub

    'Calculate cell capacity
    Public Sub calculateCellCapacity(USERS() As User)
        If Me.interesting = True Then
            For Each User In USERS
                If User.interesting = True And User.serving_basestation_ID = Me.ID Then
                    Me.cell_capacity += User.capacity
                End If
            Next
        End If
    End Sub

    'Determine sectors
    Public Sub determineSectors()
        ReDim Me.sectors(FormMain.NETWORK.sectoring_frequency_bands.Count - 1)

        sectors(0) = New Sector(FormMain.NETWORK.sectoring_frequency_bands(0),
                                Me.center_point(0),
                                Me.hexagon_verticies(0),
                                Me.hexagon_verticies(1),
                                Me.hexagon_verticies(2))

        sectors(1) = New Sector(FormMain.NETWORK.sectoring_frequency_bands(1),
                                Me.center_point(0),
                                Me.hexagon_verticies(2),
                                Me.hexagon_verticies(3),
                                Me.hexagon_verticies(4))

        sectors(2) = New Sector(FormMain.NETWORK.sectoring_frequency_bands(2),
                                Me.center_point(0),
                                Me.hexagon_verticies(4),
                                Me.hexagon_verticies(5),
                                Me.hexagon_verticies(0))
    End Sub

    'Draw on FormDrawing
    Public Sub draw()
        If Me.interesting = False Then
            'Draw plain cell
            FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawPolygon(Pens.Blue, ScalePoint(Me.hexagon_verticies, FormMain.NETWORK.scaling_factor)) 'outline
        ElseIf Me.interesting = True And Me.sectoring_group = -1 Then
            'Determine colour based on clustering group
            If clustering_group = 0 Then
                Me.colour = Brushes.LightBlue
            ElseIf clustering_group = 1 Then
                Me.colour = Brushes.LightGreen
            ElseIf clustering_group = 2 Then
                Me.colour = Brushes.LightCyan
            ElseIf clustering_group = 3 Then
                Me.colour = Brushes.LightPink
            ElseIf clustering_group = 4 Then
                Me.colour = Brushes.Lavender
            ElseIf clustering_group = 5 Then
                Me.colour = Brushes.LightSalmon
            ElseIf clustering_group = 6 Then
                Me.colour = Brushes.BlanchedAlmond
            ElseIf clustering_group = 7 Then
                Me.colour = Brushes.LightSlateGray
            End If

            'Draw on form
            FormDrawing.PanelNetworkDrawing.CreateGraphics.FillPolygon(Me.colour, ScalePoint(Me.hexagon_verticies, FormMain.NETWORK.scaling_factor), Drawing2D.FillMode.Winding) 'fill
            FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawPolygon(Pens.Blue, ScalePoint(Me.hexagon_verticies, FormMain.NETWORK.scaling_factor)) 'outline

        ElseIf Me.interesting = True And Me.sectoring_group <> -1 Then 'sectoring
            For Each Sector In Me.sectors
                'Draw on form
                FormDrawing.PanelNetworkDrawing.CreateGraphics.FillPolygon(Sector.colour, ScalePoint(Sector.verticies, FormMain.NETWORK.scaling_factor), Drawing2D.FillMode.Winding) 'fill
                FormDrawing.PanelNetworkDrawing.CreateGraphics.DrawPolygon(Pens.Gray, ScalePoint(Sector.verticies, FormMain.NETWORK.scaling_factor)) 'outline
            Next

        End If

        'Draw green basestation indicator
        Dim scaled_center_point As Point = ScalePoint(Me.center_point, FormMain.NETWORK.scaling_factor)(0)
        FormDrawing.PanelNetworkDrawing.CreateGraphics.FillRectangle(Brushes.Green, scaled_center_point.X - 1, scaled_center_point.Y - 1, 3, 3) 'green dot

    End Sub

End Class
