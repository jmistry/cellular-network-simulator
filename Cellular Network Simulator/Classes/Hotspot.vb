﻿''' <summary>
''' A class defining attributes and methods of a user hotspot
''' </summary>
''' <remarks></remarks>
Public Class Hotspot
    Public center_point(0) As Point 'in meters (x, y)
    Public radius As Double = -1 'in meters
    Public num_users As Integer = -1

    Public Sub New(ByVal cursor_position() As Point,
                   ByVal radius As Double,
                   ByVal num_users As Integer)
        Me.center_point(0) = cursor_position(0)
        Me.radius = radius
        Me.num_users = num_users
    End Sub

End Class
