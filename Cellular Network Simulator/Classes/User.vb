﻿Imports System.Math

''' <summary>
''' A class defining attributes and methods associated with a user
''' </summary>
''' <remarks></remarks>
''' 
Public Class User
    '///     Attributes     \\\'
    ''''''''''''''''''''''''''''
    'Public Variable Definitions
    Public ID As Integer
    Public center_point As Point 'center point of user in meters, referenced from top left of network area
    Public signal_power_lin As Double = 0 'mW
    Public signal_power_dBm As Double = -1000000 'very small lin power
    Public inteference_power_lin As Double = 0 'mW
    Public inteference_power_dBm As Double
    Public interesting As Boolean = False 'whether or not this user is inside an interesting basestation's cell
    Public noise_power_lin As Double = 0 'mW
    Public noise_power_dBm As Double
    Public SNR As Double = 0
    Public SIR As Double = 0
    Public SINR As Double = 0
    Public capacity As Double = 0 'bits/sec
    Public path_loss As Double = 0
    Public height As Double = 0
    Public distance_to_basestation As Double = 100000000000000000
    Public serving_basestation_ID As Integer = -1
    Public bandwidth As Bandwidth ' allocated from cell bandwidth


    '///       Methods      \\\'
    ''''''''''''''''''''''''''''
    'Constructor - used to make an instance of the class
    Public Sub New(ByVal ID As Integer,
                   ByVal location As Point,
                   ByVal height As Double)
        Me.ID = ID
        Me.center_point = location
        Me.height = height
    End Sub

    'Determine serving basestation
    Public Sub DetermineServingBasestation()
        For Each Basestation In FormMain.BASESTATIONS
            Dim distance_to_current_basestation As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
            If distance_to_current_basestation < Me.distance_to_basestation Then
                Me.distance_to_basestation = distance_to_current_basestation
                Me.serving_basestation_ID = Basestation.ID
                ArrAppend(FormMain.BASESTATIONS(Me.serving_basestation_ID).users_IDs, Me.ID)
            End If
        Next
    End Sub

    'Calculate Signal Power
    Public Sub CalculateSignalPower(ByVal path_loss_model As String)
        Select Case path_loss_model
            Case "FSPL"
                For Each Basestation In FormMain.BASESTATIONS
                    If Basestation.ID = Me.serving_basestation_ID Then
                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                        Me.path_loss = 10 * Log10(Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2))
                        Me.signal_power_dBm = 10 * Log10((Pow(10, Basestation.transmit_power_dBm / 10)) / Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2)) 'Pr = 10log10( 10^(dBm/10) / (4*pi*d*f/c)^2 )
                        Me.signal_power_lin = dBm_mW(Me.signal_power_dBm)
                    End If
                Next
            Case "3GPP"
                For Each Basestation In FormMain.BASESTATIONS
                    If Basestation.ID = Me.serving_basestation_ID Then
                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                        distance = distance / 1000 'express in km
                        Dim PL_3GPP As Double = 128.1 + 37.6 * Log10(distance)
                        Me.path_loss = PL_3GPP
                        Me.signal_power_dBm = Basestation.transmit_power_dBm - PL_3GPP
                        Me.signal_power_lin = dBm_mW(Me.signal_power_dBm)
                    End If
                Next
            Case "COST HATA"
                For Each Basestation In FormMain.BASESTATIONS
                    If Basestation.ID = Me.serving_basestation_ID Then
                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                        Dim frequency As Double = (Me.bandwidth.upper_frequency + Me.bandwidth.lower_frequency) / 2
                        Dim PL_COSTHATA As Double = COSTHata(frequency, Basestation.height, Me.height, distance, FormMain.cmbPropagationEnv.Text)
                        Me.path_loss = PL_COSTHATA
                        Me.signal_power_dBm = Basestation.transmit_power_dBm - PL_COSTHATA
                        Me.signal_power_lin = dBm_mW(Me.signal_power_dBm)
                    End If
                Next
        End Select
    End Sub

    'Calculate Interference Power
    Public Sub CalculateInterferencePower(ByVal path_loss_model As String)
        If FormMain.NETWORK.sectoring = False Then
            Dim interference As Double = 0 'linear, mW
            Select Case path_loss_model
                Case "FSPL"
                    For Each Basestation In FormMain.BASESTATIONS
                        If Basestation.ID <> Me.serving_basestation_ID And Me.bandwidth.lower_frequency >= Basestation.band.lower_frequency And Me.bandwidth.upper_frequency <= Basestation.band.upper_frequency Then
                            Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                            interference += (dBm_mW(Basestation.transmit_power_dBm) / Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2))
                        End If
                    Next
                    Me.inteference_power_lin = interference
                    Me.inteference_power_dBm = mw_dBm(interference)
                Case "3GPP"
                    For Each Basestation In FormMain.BASESTATIONS
                        If Basestation.ID <> Me.serving_basestation_ID And Me.bandwidth.lower_frequency >= Basestation.band.lower_frequency And Me.bandwidth.upper_frequency <= Basestation.band.upper_frequency Then
                            Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                            distance = distance / 1000 'express in km
                            Dim PL_3GPP As Double = 128.1 + 37.6 * Log10(distance)
                            interference += Basestation.transmit_power_dBm - PL_3GPP
                        End If
                    Next
                    Me.inteference_power_lin = interference
                    Me.inteference_power_dBm = mw_dBm(interference)
                Case "COST HATA"
                    For Each Basestation In FormMain.BASESTATIONS
                        If Basestation.ID <> Me.serving_basestation_ID And Me.bandwidth.lower_frequency >= Basestation.band.lower_frequency And Me.bandwidth.upper_frequency <= Basestation.band.upper_frequency Then

                            Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                            Dim frequency As Double = (Me.bandwidth.upper_frequency + Me.bandwidth.lower_frequency) / 2
                            Dim PL_COSTHATA As Double = COSTHata(frequency, Basestation.height, Me.height, distance, FormMain.cmbPropagationEnv.Text)
                            Me.path_loss = PL_COSTHATA
                            interference += Basestation.transmit_power_dBm - PL_COSTHATA
                        End If
                    Next
                    Me.inteference_power_lin = interference
                    Me.inteference_power_dBm = mw_dBm(interference)
            End Select
        ElseIf FormMain.NETWORK.sectoring = True Then
            'Determine which sector this user is in
            'We will need this to determine the interferers
            Dim sector_num As Integer = -1
            For Each Basestation In FormMain.BASESTATIONS
                If Basestation.ID = Me.serving_basestation_ID Then
                    For Each Sector In Basestation.sectors
                        If PointInPolygon(Sector.verticies, Me.center_point.X, Me.center_point.Y) Then
                            For i As Integer = 0 To FormMain.NETWORK.sectoring_frequency_bands.Count - 1
                                sector_num = Sector.sectorNumber
                            Next
                        End If
                    Next
                End If
            Next

            'Calculate interference
            'Consider in-band, in-direction interferers only
            Dim interference As Double = 0 'linear, mW
            Select Case path_loss_model
                Case "FSPL"
                    For Each Basestation In FormMain.BASESTATIONS
                        If Basestation.ID <> Me.serving_basestation_ID Then
                            Select Case sector_num
                                Case 0 'bottom left sector, look for interferers from top right
                                    If Basestation.center_point(0).X >= FormMain.BASESTATIONS(Me.serving_basestation_ID).center_point(0).X And
                                        Basestation.center_point(0).Y <= FormMain.BASESTATIONS(Me.serving_basestation_ID).center_point(0).Y Then

                                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                                        interference += (dBm_mW(Basestation.transmit_power_dBm) / Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2))

                                    End If
                                Case 1 'top left sector, look for interferers from bottom right
                                    If Basestation.center_point(0).X >= FormMain.BASESTATIONS(Me.serving_basestation_ID).center_point(0).X And
                                        Basestation.center_point(0).Y >= FormMain.BASESTATIONS(Me.serving_basestation_ID).center_point(0).Y Then

                                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                                        interference += (dBm_mW(Basestation.transmit_power_dBm) / Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2))

                                    End If
                                Case 2 'right sector, look for interferers from left
                                    If Basestation.center_point(0).X < FormMain.BASESTATIONS(Me.serving_basestation_ID).center_point(0).X Then

                                        Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                                        interference += (dBm_mW(Basestation.transmit_power_dBm) / Pow((4 * PI * distance * FormMain.NETWORK.carrier_frequency) / FormMain.speed_of_light, 2))

                                    End If
                            End Select

                        End If
                    Next
                    Me.inteference_power_lin = interference
                    Me.inteference_power_dBm = mw_dBm(interference)
                Case "3GPP"
                    For Each Basestation In FormMain.BASESTATIONS
                        If Basestation.ID <> Me.serving_basestation_ID And Me.bandwidth.lower_frequency >= Basestation.band.lower_frequency And Me.bandwidth.upper_frequency <= Basestation.band.upper_frequency Then
                            Dim distance As Double = Sqrt(Pow(Me.center_point.X - Basestation.center_point(0).X, 2) + Pow(Me.center_point.Y - Basestation.center_point(0).Y, 2))
                            distance = distance / 1000 'express in km
                            Dim PL_3GPP As Double = 125.2 + 36.3 * Log10(distance)
                            interference += Basestation.transmit_power_dBm - PL_3GPP
                        End If
                    Next
                    Me.inteference_power_lin = interference
                    Me.inteference_power_dBm = mw_dBm(interference)
            End Select
        End If
    End Sub

    'Determine if user is inside interesting cell (must run CalculateSignalPower() first)
    Public Sub DetermineIfInteresting(BASESTATIONS() As Basestation)
        For Each currentBasestation In BASESTATIONS
            If currentBasestation.ID = Me.serving_basestation_ID And currentBasestation.interesting = True Then
                Me.interesting = True
            End If
        Next
    End Sub

    'Calculate noise power (Thermal, Johnson-Nyquist Noise)
    Public Sub CalculateNoisePower(ByVal temp_K As Double)
        If Me.interesting = True Then
            Me.noise_power_lin = FormMain.Boltzmann_const * temp_K * Me.bandwidth.bandwidth * Pow(10, 8 / 10) 'N = kTB + NF (NF = 8dB)
            Me.noise_power_dBm = 10 * Log10(noise_power_lin)
        End If
    End Sub

    'Calculate SIR
    Private Sub CalculateSIR()
        Me.SIR = Me.signal_power_dBm - Me.inteference_power_dBm
    End Sub

    'Calculate SNR
    Private Sub CalculateSNR()
        Me.SNR = Me.signal_power_dBm - Me.noise_power_dBm
    End Sub

    'Calculate SINR
    Private Sub CalculateSINR()
        Me.SINR = 10 * Log10(Me.signal_power_lin / (Me.inteference_power_lin + Me.noise_power_lin))
    End Sub

    'Calculate Capacity - Shannon approximation
    Public Sub CalculateCapacity()
        If Me.interesting = True Then
            Me.CalculateSIR()
            Me.CalculateSNR()
            Me.CalculateSINR()
            Me.capacity = Me.bandwidth.bandwidth * Log(1 + Tools.dB_lin(SINR), 2)
            If FormMain.NETWORK.sectoring = True Then
                Me.capacity = Me.capacity / 10
            End If
        End If
    End Sub

End Class
