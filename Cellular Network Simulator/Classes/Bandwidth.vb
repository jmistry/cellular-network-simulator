﻿''' <summary>
''' A class defining a frequency band (Hz)
''' </summary>
''' <remarks></remarks>
Public Class Bandwidth
    '///     Attributes     \\\'
    ''''''''''''''''''''''''''''
    'Public Variable Definitions
    'All values in Hz
    Public upper_frequency As Double
    Public lower_frequency As Double
    Public bandwidth As Double

    '///       Methods      \\\'
    ''''''''''''''''''''''''''''
    'Constructor - used to make an instance of the class
    Public Sub New(ByVal upper_frequency As Double, ByVal lower_frequency As Double)
        Me.upper_frequency = upper_frequency
        Me.lower_frequency = lower_frequency
        Me.bandwidth = upper_frequency - lower_frequency
    End Sub

End Class