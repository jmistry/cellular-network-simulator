﻿Imports System.Math

''' <summary>
''' A class defining attributes and methods associated with the Network
''' </summary>
''' <remarks></remarks>
Public Class Network
    '///     Attributes     \\\'
    ''''''''''''''''''''''''''''
    'Public Variable Definitions
    Public size As Point 'size of network (x,y) in meters
    Public basestation_separation As Double 'meters
    Public user_count As Integer = 0
    Public bandwidth As Double
    Public basestation_count As Integer
    Public basestation_count_x As Integer
    Public basestation_count_y As Integer
    Public scaling_factor As Double 'drawing scaling factor for appropriately sized graphics
    Public user_area As Rectangle 'defines the area over which users are distributed
    Public capacity As Double = 0 'bits/sec
    Public carrier_frequency As Double
    Public clustering_cluster_size_N As Integer = 1 'number of frequency groups in clustering
    Public clustering_frequency_bands() As Bandwidth 'clustering bands
    Public sectoring_frequency_bands() As Bandwidth 'sectoring bands
    Public clustering As Boolean = False
    Public sectoring As Boolean = False
    Public outage_probability As Double = -1

    '///       Methods      \\\'
    ''''''''''''''''''''''''''''
    'Constructor - used to make an instance of the class
    Public Sub New()
    End Sub

    'Calculate number of basestations
    Public Sub CalculateNumBasestations()
        basestation_count_x = (size.X / basestation_separation) * (4 / 3)
        basestation_count_y = (size.Y / basestation_separation) / 0.866
        basestation_count = basestation_count_x * basestation_count_y
    End Sub

    'Calculate scaling factor for drawing
    Public Sub CalculateScalingFactor()
        Me.scaling_factor = FormDrawing.Width / size.X '-45 to account for edges of form
    End Sub

    'Calculate network statistics
    Public Sub CalculateNetworkStats(BASESTATIONS() As Basestation, USERS() As User)
        'Calculate capacity within each cell
        For Each Basestation In BASESTATIONS
            Basestation.calculateCellCapacity(USERS)
        Next

        'Calculate network capacity
        For Each User In USERS
            If User.interesting = True Then
                Me.capacity += User.capacity
            End If
        Next

        'Calculate outage probabilty
        Dim threshold As Double = FormMain.txtMinThroughput.Text * Pow(10, 6)
        Dim num_users_good_throughput As Integer = 0
        Dim num_users_bad_throughput As Integer = 0
        For Each User In USERS
            If User.interesting = True Then
                If User.capacity >= threshold Then
                    num_users_good_throughput += 1
                ElseIf User.capacity < threshold Then
                    num_users_bad_throughput += 1
                End If
            End If
        Next
        Me.outage_probability = (100 * num_users_bad_throughput / (num_users_bad_throughput + num_users_good_throughput))

        'Update Network Stats form
        FormNetworkStats.UpdateAllStats()
    End Sub

End Class
